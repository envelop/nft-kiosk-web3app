## Docker Dev Environment with local NGINX
```bash
docker run --rm  -v $PWD:/app node:19 /bin/bash -c 'cd /app && npm install --force && chmod -R 777 node_modules'
```
```bash
docker run --rm  -v $PWD:/app node:19 /bin/bash -c 'cd /app && npm run build'
```

```bash
docker-compose -f docker-compose-local.yaml up
```
http://localhost:8080/launchpad

### Node build info

```
> node --version
v19.4.0

> npm -V
9.2.0
```

```
> npm install --force
> npm run build
```

Для 19 версии ноды, в package-json перед задачей должна быть переменная окружения `NODE_OPTIONS=--openssl-legacy-provider`

И отдать нжинксом все, что есть в `/build`

Пока ворнингов нет, но если билд не будет собираться из-за них, то в `package.json` поменять (30 строка):

```
"build": "NODE_OPTIONS=--openssl-legacy-provider react-scripts build",
```

на

```
"build": "CI=false; NODE_OPTIONS=--openssl-legacy-provider react-scripts build",
```