
import {
	WrappedTokenType,
	ChainParamsType,
	ERC20ParamsType,
	DiscountUntil,
	SubscriptionTariff,
	Discount,
	Price
} from '../models/BlockchainAdapter';
import {
	AdvancedLoaderStageType,
	AdvancedLoaderType
} from './reducer';

import BigNumber from 'bignumber.js';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

// ---------- NAVIGATION ----------
export const setSelectedChain = (payload: { chainId: number }) => {
	return {
		type: 'SET_SELECTED_CHAIN',
		payload,
	}
}
export const setLaunchpadContract = (payload: { launchpadContract: string, displayName: string }) => {
	return {
		type: 'SET_LAUNCHPAD_CONTRACT',
		payload,
	}
}
export const setLoading = (payload: { msg: string }) => {
	return {
		type: 'SET_LOADING',
		payload,
	}
}
export const unsetLoading = () => {
	return {
		type: 'UNSET_LOADING',
	}
}
export const createAdvancedLoading = (payload: AdvancedLoaderType) => {
	return {
		type: 'CREATE_ADVANCED_LOADING',
		payload,
	}
}
export const clearAdvancedLoading = () => {
	return {
		type: 'CLEAR_ADVANCED_LOADING',
	}
}
export const updateStepAdvancedLoading = (payload: AdvancedLoaderStageType) => {
	return {
		type: 'UPDATE_STEP_ADVANCED_LOADING',
		payload,
	}
}
export const setError = (payload: {
	title?: string,
	text  : string | Array<string>,
	buttons: undefined | Array<{
		text: string,
		clickFunc: Function,
	}>,
	links: undefined | Array<{
		text: string,
		url : string,
	}>
}) => {
	return {
		type: 'SET_ERROR',
		payload,
	}
}
export const clearError = () => {
	return {
		type: 'CLEAR_ERROR',
	}
}
export const setInfo = (payload: {
	title?: string,
	text  : string | Array<string>,
	buttons: undefined | Array<{
		text: string,
		clickFunc: Function,
	}>,
	links: undefined | Array<{
		text: string,
		url : string,
	}>
}) => {
	return {
		type: 'SET_INFO',
		payload,
	}
}
export const clearInfo = () => {
	return {
		type: 'CLEAR_INFO',
	}
}
export const resetAppData = () => {
	return {
		type: 'RESET_APP_DATA',
	}
}
export const gotoListRequest = () => {
	return {
		type: 'GOTO_LIST_REQUEST',
	}
}
export const gotoListResolve = () => {
	return {
		type: 'GOTO_LIST_RESOLVE',
	}
}
export const setDisplayTitle = (payload: {
	displayTitle      : string,
	displayDescription: string,
	showWidget        : boolean,
}) => {
	return {
		type: 'SET_DISPLAY_TTILE',
		payload,
	}
}
// ---------- END NAVIGATION ----------

// ---------- CONNECTION ----------
export const metamaskConnectionSuccess = (payload: { address: string }) => {
	return {
		type: 'METAMASK_CONNECTION_SUCCESS',
		payload,
	}
}
export const metamaskConnectionNotInstalled = () => {
	return {
		type: 'METAMASK_CONNECTION_NOT_INSTALLED',
	}
}
export const metamaskConnectionRejected = () => {
	return {
		type: 'METAMASK_CONNECTION_REJECTED',
	}
}
export const metamaskSetChainParams = (
	payload: ChainParamsType
) => {
	return {
		type: 'METAMASK_SET_CHAIN_PARAMS',
		payload,
	}
}
export const metamaskSetAvailableChains = ( payload: Array<ChainParamsType> ) => {
	return {
		type: 'METAMASK_SET_AVAILABLE_CHAINS',
		payload,
	}
}
export const setAuthMethod = ( payload: string ) => {
	return {
		type: 'SET_AUTH_METHOD',
		payload,
	}
}
export const unsetAuthMethod = () => {
	return {
		type: 'UNSET_AUTH_METHOD',
	}
}
export const requestChain = ( payload: number | undefined ) => {
	return {
		type: 'REQUEST_CHAIN',
		payload
	}
}
// ---------- END CONNECTION ----------

// ---------- NATIVE TOKEN ----------
export const updateNativeBalance = ( payload: { balance: BigNumber } ) => {
	return {
		type: 'UPDATE_NATIVE_BALANCE',
		payload,
	}
}
export const addERC20Pending = ( payload: string ) => {
	return {
		type: 'ADD_ERC20_PENDING',
		payload,
	}
}
export const updateERC20ContractParams = ( payload: ERC20ParamsType | {
	address: string,
	balance: BigNumber,
	allowance: BigNumber,
} ) => {
	return {
		type: 'UPDATE_ERC20_CONTRACT_PARAMS',
		payload,
	}
}
// ---------- END NATIVE TOKEN ----------

// ---------- ERC721 FETCH ----------
export const wrappedTokensAdd = (payload: WrappedTokenType) => {
	return {
		type: 'WRAPPED_TOKENS_ADD',
		payload,
	}
}
export const wrappedTokensUpdatePrice = (payload: {
	contractAddress: string,
	tokenId: string,
	prices: Array<Price>,
	discounts: Array<Discount>,
}) => {
	return {
		type: 'WRAPPED_TOKENS_UPDATE_PRICE',
		payload,
	}
}
export const wrappedTokensRemove = (payload: WrappedTokenType) => {
	return {
		type: 'WRAPPED_TOKENS_REMOVE',
		payload,
	}
}
export const wrappedTokensClear = () => {
	return {
		type: 'WRAPPED_TOKENS_CLEAR',
	}
}
export const setNoTokens = () => {
	return {
		type: 'SET_NO_TOKENS',
	}
}
export const setTokensLoading = () => {
	return {
		type: 'SET_TOKENS_LOADING',
	}
}
export const unsetTokensLoading = () => {
	return {
		type: 'UNSET_TOKENS_LOADING',
	}
}
export const addDisplayDiscount = (payload: DiscountUntil) => {
	return {
		type: 'ADD_DISPLAY_DISCOUNT',
		payload
	}
}
export const clearDisplayDiscount = () => {
	return {
		type: 'CLEAR_DISPLAY_DISCOUNT',
	}
}
// ---------- END ERC721 FETCH ----------

// ---------- SAFT ----------
export const updateSubscription = (payload: {
	subscription: undefined | {
		timeRemaining: BigNumber,
		txRemaining: number,
	}
}) => {
	return {
		type: 'SAFT_UPDATE_SUBSCRIPTION',
		payload,
	}
}
export const setSubscriptionsTariffs = (payload: { tariffs: Array<SubscriptionTariff> }) => {
	return {
		type: 'SAFT_SET_TARIFFS',
		payload,
	}
}
// ---------- END WL BL ----------
