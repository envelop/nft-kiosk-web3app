
import {
	reducer,
	initialState,
	_AdvancedLoadingStatus
} from './reducer';

import type {
	AdvancedLoaderType,
	AdvancedLoaderStageType,
} from './reducer'

import {
	setSelectedChain,
	setLaunchpadContract,

	createAdvancedLoading,
	clearAdvancedLoading,
	updateStepAdvancedLoading,
	setLoading,
	unsetLoading,
	setError,
	clearError,
	setInfo,
	clearInfo,
	resetAppData,
	gotoListRequest,
	gotoListResolve,

	metamaskConnectionSuccess,
	metamaskConnectionNotInstalled,
	metamaskConnectionRejected,
	metamaskSetChainParams,
	metamaskSetAvailableChains,
	setAuthMethod,
	unsetAuthMethod,
	requestChain,

	updateNativeBalance,
	addERC20Pending,
	updateERC20ContractParams,

	wrappedTokensAdd,
	wrappedTokensUpdatePrice,
	wrappedTokensRemove,
	wrappedTokensClear,
	setNoTokens,
	setTokensLoading,
	unsetTokensLoading,
	addDisplayDiscount,
	clearDisplayDiscount,

	updateSubscription,
	setSubscriptionsTariffs,
	setDisplayTitle,
} from './actions';

export {
	reducer,
	initialState,

	_AdvancedLoadingStatus,

	setSelectedChain,
	setLaunchpadContract,

	createAdvancedLoading,
	clearAdvancedLoading,
	updateStepAdvancedLoading,
	setLoading,
	unsetLoading,
	setError,
	clearError,
	setInfo,
	clearInfo,
	resetAppData,
	gotoListRequest,
	gotoListResolve,

	metamaskConnectionSuccess,
	metamaskConnectionNotInstalled,
	metamaskConnectionRejected,
	metamaskSetChainParams,
	metamaskSetAvailableChains,
	setAuthMethod,
	unsetAuthMethod,
	requestChain,

	updateNativeBalance,
	addERC20Pending,
	updateERC20ContractParams,
	updateSubscription,
	setSubscriptionsTariffs,

	wrappedTokensAdd,
	wrappedTokensUpdatePrice,
	wrappedTokensRemove,
	wrappedTokensClear,

	addDisplayDiscount,
	clearDisplayDiscount,

	setNoTokens,
	setTokensLoading,
	unsetTokensLoading,
	setDisplayTitle,
}

export type {
	AdvancedLoaderType,
	AdvancedLoaderStageType,
}