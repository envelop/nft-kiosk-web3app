
import {
	ERC20ParamsType,
	WrappedTokenType,
	ChainParamsType,
	DiscountUntil,
	SubscriptionTariff,
} from '../models/BlockchainAdapter';

import BigNumber from 'bignumber.js';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

export enum _AdvancedLoadingStatus {
	queued,
	loading,
	complete,
};
export type AdvancedLoaderType = {
	title: string,
	stages: Array<AdvancedLoaderStageType>
};
export type AdvancedLoaderStageType = {
	id: string,
	sortOrder: number,
	current?: number,
	total?: number,
	text: string,
	status: _AdvancedLoadingStatus
};

type StateType = {
	launchpadContract : string,
	displayName       : string,
	displayTitle      : string,
	displayDescription: string,
	showWidget        : boolean,
	_loading          : string,
	_advancedLoading  : undefined | AdvancedLoaderType,
	_error            : undefined | {
		title? : string,
		text   : string | Array<string>,
		buttons: undefined | Array<{
			text     : string,
			clickFunc: Function,
		}>,
		links: undefined | Array<{
			text: string,
			url : string,
		}>,
	},
	_info    : undefined | {
		text   : string | Array<string>,
		buttons: undefined | Array<{
			text     : string,
			clickFunc: Function,
		}>,
		links: undefined | Array<{
			text: string,
			url : string,
		}>,
	},
	account: {
		address      : String,
		balanceNative: BigNumber,
	},
	selectedChain: number,
	// selectedUser : string,
	metamaskAdapter: {
		logged              : Boolean,
		metamaskNotInstalled: Boolean,
		permissionRejected  : Boolean,
		chainId             : Number,
		requestChainId      : number | undefined,
		availableChains     : Array<ChainParamsType>,
		authMethod          : string,
	},
	erc20Tokens               : Array<ERC20ParamsType>,
	erc20TokensPending        : Array<string>,
	tokensOnDisplay           : Array<WrappedTokenType>,
	displayDiscount           : undefined | DiscountUntil,
	loadInProgress            : boolean,
	noTokens                  : boolean;
	subscriptionBought        : undefined | {
		timeRemaining: BigNumber,
		txRemaining: number,
	},
	subscriptionAvailableTariffs: Array<SubscriptionTariff>,
};

export const initialState: StateType = {
	launchpadContract : '',
	displayName       : '',
	displayTitle      : '',
	displayDescription: '',
	showWidget        : false,
	_loading          : '',
	_advancedLoading  : undefined,
	_error            : undefined,
	_info             : undefined,
	account           : {
		address       : '',
		balanceNative : new BigNumber(0),
	},
	selectedChain: 0,
	metamaskAdapter: {
		logged              : false,
		metamaskNotInstalled: false,
		permissionRejected  : false,
		chainId             : 0,
		requestChainId      : undefined,
		availableChains     : [],
		authMethod          : '',
	},
	erc20Tokens                 : [],
	erc20TokensPending          : [],
	displayDiscount             : undefined,
	tokensOnDisplay             : [],
	loadInProgress              : false,
	noTokens                    : false,
	subscriptionBought          : undefined,
	subscriptionAvailableTariffs: [],
}

export const reducer = (state = initialState, action: any): StateType => {

	switch ( action.type ) {

		// ---------- NAVIGATION ----------
		case 'SET_SELECTED_CHAIN': {
			return {
				...state,
				selectedChain: action.payload.chainId
			}
		}
		case 'SET_LAUNCHPAD_CONTRACT': {
			return {
				...state,
				launchpadContract: action.payload.launchpadContract,
				displayName: action.payload.displayName,
			}
		}
		case 'SET_LOADING': {
			return {
				...state,
				_loading: action.payload.msg,
			}
		}
		case 'UNSET_LOADING': {
			return {
				...state,
				_loading: '',
				_advancedLoading: undefined,
			}
		}
		case 'CREATE_ADVANCED_LOADING': {
			return {
				...state,
				_advancedLoading: action.payload,
			}
		}
		case 'CLEAR_ADVANCED_LOADING': {
			return {
				...state,
				_advancedLoading: undefined,
			}
		}
		case 'UPDATE_STEP_ADVANCED_LOADING': {
			if ( !state._advancedLoading ) { return state; }

			return {
				...state,
				_advancedLoading: {
					...state._advancedLoading,
					stages: [
						...state._advancedLoading.stages.filter((item) => {
							return item.id.toLowerCase() !== action.payload.id.toLowerCase()
						}),
						action.payload
					]
				},
			}
		}
		case 'SET_ERROR': {
			return {
				...state,
				_error: action.payload,
			}
		}
		case 'CLEAR_ERROR': {
			return {
				...state,
				_error: undefined,
			}
		}
		case 'SET_INFO': {
			return {
				...state,
				_info: action.payload,
			}
		}
		case 'CLEAR_INFO': {
			return {
				...state,
				_info: undefined,
			}
		}
		case 'RESET_APP_DATA': {
			return {
				...initialState,
				metamaskAdapter: {
					...initialState.metamaskAdapter,
					availableChains: state.metamaskAdapter.availableChains,
				}
			};
		}
		case 'SET_DISPLAY_TTILE': {
			return {
				...state,
				displayTitle: action.payload.displayTitle,
				displayDescription: action.payload.displayDescription,
				showWidget: action.payload.showWidget,
			}
		}
		// ---------- END NAVIGATION ----------
		// ---------- CONNECTION ----------
		case 'METAMASK_CONNECTION_SUCCESS': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					metamaskNotInstalled: false,
					permissionRejected  : false,
					logged              : true,
				},
				account: {
					...state.account,
					address: action.payload.address,
				}
			}
		}
		case 'METAMASK_CONNECTION_NOT_INSTALLED': {
			return {
				...state,
				metamaskAdapter: {
					...initialState.metamaskAdapter,
					metamaskNotInstalled: true,
				}
			}
		}
		case 'METAMASK_CONNECTION_REJECTED': {
			return {
				...state,
				metamaskAdapter: {
					...initialState.metamaskAdapter,
					permissionRejected: true,
					authMethod: state.metamaskAdapter.authMethod,
				}
			}
		}
		case 'METAMASK_SET_CHAIN_PARAMS': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					...action.payload,
				},
			}
		}
		case 'METAMASK_SET_AVAILABLE_CHAINS': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					availableChains: action.payload,
				}
			}
		}
		case 'SET_AUTH_METHOD': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					authMethod: action.payload,
				}
			}
		}
		case 'UNSET_AUTH_METHOD': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					authMethod: '',
				}
			}
		}
		case 'REQUEST_CHAIN': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					requestChainId: action.payload,
				}
			}
		}
		// ---------- END CONNECTION ----------

		// ---------- NATIVE TOKEN ----------
		case 'UPDATE_NATIVE_BALANCE': {
			return {
				...state,
				account: {
					...state.account,
					balanceNative: action.payload.balance,
				}
			}
		}
		// ---------- END NATIVE TOKEN ----------

		// ---------- ERC20 CONTRACT ----------
		case 'ADD_ERC20_PENDING': {
			if ( state.erc20TokensPending.includes(action.payload.toLowerCase()) ) {
				return state;
			}
			return {
				...state,
				erc20TokensPending: [
					...state.erc20TokensPending,
					action.payload.toLowerCase()
				],
			}
		}
		case 'UPDATE_ERC20_CONTRACT_PARAMS': {
			return {
				...state,
				erc20Tokens: [
					...state.erc20Tokens.filter((item) => { return item.address.toLowerCase() !== action.payload.address.toLowerCase() }),
					action.payload
				],
				erc20TokensPending: state.erc20TokensPending.filter((item) => { return item !== action.payload.address.toLowerCase() }),
			}
		}
		// ---------- END ERC20 CONTRACT ----------

		// ---------- ERC721 FETCH ----------
		case 'WRAPPED_TOKENS_ADD': {
			return {
				...state,
				tokensOnDisplay: [
					...state.tokensOnDisplay.filter((item) => { return item.contractAddress !== action.payload.contractAddress || item.tokenId !== action.payload.tokenId }),
					action.payload,
				]
			}
		}
		case 'WRAPPED_TOKENS_UPDATE_PRICE': {
			let foundToken = state.tokensOnDisplay.find((item) => { return item.contractAddress === action.payload.contractAddress && item.tokenId === action.payload.tokenId });
			foundToken = {
				...foundToken,
				...action.payload
			}
			if ( !foundToken ) { return state; }
			return {
				...state,
				tokensOnDisplay: [
					...state.tokensOnDisplay.filter((item) => { return item.contractAddress !== action.payload.contractAddress || item.tokenId !== action.payload.tokenId }),
					foundToken,
				]
			}
		}
		case 'WRAPPED_TOKENS_REMOVE': {
			return {
				...state,
				tokensOnDisplay: [
					...state.tokensOnDisplay.filter((item) => { return item.contractAddress !== action.payload.contractAddress || item.tokenId !== action.payload.tokenId }),
				]
			}
		}
		case 'WRAPPED_TOKENS_CLEAR': {
			return {
				...state,
				tokensOnDisplay: initialState.tokensOnDisplay,
			}
		}
		case 'SET_NO_TOKENS': {
			return {
				...state,
				loadInProgress: false,
				noTokens: true,
			}
		}
		case 'UNSET_TOKENS_LOADING': {
			return {
				...state,
				loadInProgress: false,
			}
		}
		case 'SET_TOKENS_LOADING': {
			return {
				...state,
				loadInProgress: true,
			}
		}

		case 'ADD_DISPLAY_DISCOUNT': {
			return {
				...state,
				displayDiscount: action.payload,
			}
		}
		case 'CLEAR_DISPLAY_DISCOUNT': {
			return {
				...state,
				displayDiscount: undefined
			}
		}
		// ---------- END ERC721 FETCH ----------

		// ---------- SAFT ----------
		case 'UPDATE_SUBSCRIPTION': {
			return {
				...state,
				subscriptionBought: action.payload.subscription
			}
		}
		case 'SAFT_SET_TARIFFS': {
			return {
				...state,
				subscriptionAvailableTariffs: action.payload.tariffs
			}
		}
		// ---------- END SAFT ----------

		default: { return state }

	}
}