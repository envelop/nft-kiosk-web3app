
import { default as urljoin } from 'url-join';

import default_icon from '../../static/pics/coins/_default.svg';
import config from '../../config.json';

import {
	WrappedTokenType,
	_AssetType,
	decodeAssetTypeFromIndex,
	decodeWrappedToken,
	_AssetItem,
	_Fee,
	_Lock,
	_Royalty,
	getDisplayHash,
	_Price,
	_Discount,
	decodeCollaterals,
	decodePrice,
	decodeDiscount,
	MetamaskAdapter,
	decodeDiscountUntil,
	ERC20ParamsType,
	decodeLocks,
	_DiscountUntil,
	DiscountUntil
} from '../BlockchainAdapter';

import { processSwarmUrl } from '../_utils';

import {
	addDisplayDiscount,
	addERC20Pending,
	setDisplayTitle,
	setError,
	setTokensLoading,
	unsetTokensLoading,
	updateERC20ContractParams,
	wrappedTokensAdd
} from '../../reducers';

import {
	getERC20Balance,
	getERC20Params,
} from '../BlockchainAdapter';

import BigNumber from 'bignumber.js';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

export type DiscoveredToken = {
	blocknumber: BigNumber,
	logindex: number,
	contract_address: string,
	token_id: string,
	owner: string,
	token_uri: string,
	asset_type: string,
	is_wnft?: boolean,
	balance?: string,
	in_contract_address?: string,
	in_token_id?: string,
	in_asset_type?: string,
	totalSupply?: string
}

export type APIWrappedToken = {
    collateral_json: Array<{
		amount: number,
		tokenId: number,
		assetType: number,
		contractAddress: string
	}>,
    contract_address: string,
    token_id: string,
    token_uri: string,
    owner: string,
    blocknumber: BigNumber,
    logindex: BigNumber,
    wnft_type: _AssetType,
	balance: number | undefined,
    initial_out_balance: BigNumber,
    in_asset_type: number,
    in_contract_address: string,
    in_token_id: string,
    in_amount: BigNumber,
    unwrap_destination: string,
    rules: string,
    is_burned: boolean,
    fees: Array<_Fee>,
    locks: Array<_Lock>,
    royalties: Array<_Royalty>,
    first_owner: string,
    create_tx: string,
    burn_tx: string,
    inserted: string,
    updated: string,
    updated_by: string,
    asset_type: _AssetType,
    is_wnft: boolean,
	totalSupply: string | undefined,
}
export type APIKioskItem = {
	id: string,
	chain_id: number,
	contract_address: string,
	token_id: string,
	asset_type: number,
	display_id: string,
	prices: Array<_Price>,
	discounts: Array<_Discount>,
	status: string,
	sell_tx_hash: string,
	blocknumber: string,
	logindex: string,
	implicit_added: boolean,
	default_display_id: string,
	kiosk_address: string,
	token_uri: string,
	default_display_name_hash: string,
	default_display_name: string,
	name_hash: string,
	name: string,
	collateral: Array<_AssetItem>,
	locks: Array<_Lock>,
}
export type APIERC20Item = {
	chain_id: number,
	contract_address: string,
	contract_type: string,
	abi: string,
	name: string,
	symbol: string,
	decimals: string,
}

const parseAPIToken = async (item: APIWrappedToken, chainId: number): Promise<WrappedTokenType> => {
	const inAsset = {
		asset: {
			assetType: decodeAssetTypeFromIndex(`${item.in_asset_type}`),
			contractAddress: item.in_contract_address,
		},
		tokenId: `${item.in_token_id}`,
		amount: `${item.in_amount}`,
	};

	let collateral: Array<_AssetItem> = [];
	if ( item.collateral_json ) {
		collateral = item.collateral_json.map((iitem): _AssetItem => {
			return {
				asset: {
					assetType: decodeAssetTypeFromIndex(`${iitem.assetType}`),
					contractAddress: iitem.contractAddress,
				},
				tokenId: `${iitem.tokenId}`,
				amount: `${iitem.amount}`,
			}
		})
	}

	let tokenUrl;
	let tokenUrlRaw;
	let tokenUrlRawJSON = '';

	if ( item.token_uri ) {
		try {
			tokenUrlRaw = item.token_uri;
			tokenUrl = await fetch(processSwarmUrl(item.token_uri));
			tokenUrlRawJSON = await tokenUrl.text();
		} catch(e: any) {
			console.log('Cannot fetch tokenURL', e)
		}
	}

	const amount      = item.balance ? new BigNumber(item.balance) : undefined;
	const totalSupply = item.totalSupply ? new BigNumber(item.totalSupply) : undefined;

	return decodeWrappedToken({
		inWNFT: {
			inAsset,
			collateral,
			unWrapDestinition: item.unwrap_destination,
			unWrapDestination: item.unwrap_destination,
			fees: item.fees,
			locks: item.locks,
			royalties: item.royalties,
			rules: item.rules,
		},
		owner: item.owner,
		chainId: chainId,
		contractAddress: item.contract_address,
		tokenId: item.token_id,
		assetType: item.asset_type,
		amount,
		totalSupply,
		tokenUrl: item.token_uri,
		tokenUrlRaw: tokenUrlRaw || '',
		tokenUrlRawJSON: tokenUrlRawJSON
	})
}
const parseAPITokens = async (tokens: Array<APIWrappedToken>, chainId: number): Promise<Array<WrappedTokenType>> => {
	return await Promise.all(tokens.map((item: APIWrappedToken): Promise<WrappedTokenType> => { return parseAPIToken(item, chainId) }));
}

const createAuthToken = async () => {

	async function sha256(message: string) {
		const msgBuffer = new TextEncoder().encode(message);
		const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);
		const hashArray = Array.from(new Uint8Array(hashBuffer));
		const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
		return hashHex;
	}
	async function signTimed(name: string, key: string) {
		const now = new Date().getTime();
		const timeBlock = parseInt(`${now / (parseInt(key_active || '0') * 1000)}`);

		return sha256(name+key+timeBlock)
	}

	const app_name   = window.location.host;
	const app_id     = process.env.REACT_APP_ORACLE_APP_ID;
	const app_key    = process.env.REACT_APP_ORACLE_APP_KEY;
	const key_active = process.env.REACT_APP_ORACLE_KEY_ACTIVE_TIME;

	if ( !app_id || !app_key || !key_active ) {
		console.log('No app_id or app_key of key_active in .env');
		return '';
	}
	const tempKey = await signTimed(app_name, app_key);

	return `${app_id}.${tempKey}`
}

export const fetchUserTokens = async (chainId: number, assetType: _AssetType, userAddress: string, page: number):Promise<Array<DiscoveredToken>> => {

	const tokensOnPage = 12

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		return [];
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return []; }

	let url = '';
	if ( assetType === _AssetType.ERC721 ) {
		url = urljoin(BASE_URL, `/discover/721/user/${chainId}/${userAddress}?page=${page}&size=${tokensOnPage}`);
	}
	if ( assetType === _AssetType.ERC1155 ) {
		url = urljoin(BASE_URL, `/discover/1155/user/${chainId}/${userAddress}?page=${page}&size=${tokensOnPage}`);
	}

	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		const respParsed: Array<DiscoveredToken> = await resp.json();
		if ( !resp.ok ) {
			console.log('Cannot fetch token from oracle', respParsed);
			return [];
		}
		if ('error' in respParsed) {
			return [];
		}

		return respParsed;
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		return [];
	}
}
export const fetchUserTokensOfContract = async (chainId: number, assetType: _AssetType, userAddress: string, contractAddress: string, page: number):Promise<Array<DiscoveredToken>> => {

	const tokensOnPage = 12

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		return [];
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return []; }

	let url = '';
	if ( assetType === _AssetType.ERC721 ) {
		url = urljoin(BASE_URL, `/discover/721/user/${chainId}/${userAddress}/${contractAddress}?page=${page}&size=${tokensOnPage}`);
	}
	if ( assetType === _AssetType.ERC1155 ) {
		url = urljoin(BASE_URL, `/discover/1155/user/${chainId}/${userAddress}/${contractAddress}?page=${page}&size=${tokensOnPage}`);
	}

	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		const respParsed: Array<DiscoveredToken> = await resp.json();
		if ( !resp.ok ) {
			console.log('Cannot fetch token from oracle', respParsed);
			return [];
		}
		if ('error' in respParsed) {
			return [];
		}

		return respParsed;
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		return [];
	}
}

export const fetchUserWrappedTokens = async (chainId: number, assetType: _AssetType, userAddress: string, page: number):Promise<Array<WrappedTokenType>> => {

	const tokensOnPage = 12

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		return [];
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return []; }

	let url = '';
	if ( assetType === _AssetType.ERC721 ) {
		url = urljoin(BASE_URL, `/wnft_collateral/721/user/${chainId}/${userAddress}?page=${page}&size=${tokensOnPage}`);
	}
	if ( assetType === _AssetType.ERC1155 ) {
		url = urljoin(BASE_URL, `/wnft_collateral/1155/user/${chainId}/${userAddress}?page=${page}&size=${tokensOnPage}`);
	}

	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		const respParsed: Array<APIWrappedToken> = await resp.json();
		if ( !resp.ok ) {
			console.log('Cannot fetch token from oracle', respParsed);
			return [];
		}
		if ('error' in respParsed) {
			return [];
		}


		return await parseAPITokens(respParsed, chainId);
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		return [];
	}
}

export const fetchWrappedTokenById = async (params: {
	chainId: number,
	contractAddress: string,
	tokenId: string,
	userAddress?: string,
	assetType?: _AssetType
}):Promise<WrappedTokenType | undefined> => {

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		return undefined;
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return undefined; }

	const url721 = urljoin(BASE_URL, `/wnft_collateral/721/${params.chainId}/${params.contractAddress}/${params.tokenId}`);
	const url1155 = urljoin(BASE_URL, `/wnft_collateral/1155/${params.chainId}/${params.contractAddress}/${params.tokenId}`);

	let respParsed;

	let url = '';
	if ( params.assetType ) {
		if ( params.assetType === _AssetType.ERC721 ) {
			url = url721;
		}
		if ( params.assetType === _AssetType.ERC1155 ) {
			url = url1155;
		}

		try {
			const resp = await fetch(url, {
				headers: {
					'Authorization': authToken,
				}
			});
			respParsed = await resp.json();
		} catch (e) {
			console.log('Cannot fetch token from oracle', e);
			return undefined;
		}
	} else {

		try {
			const resp = await fetch(url721, {
				headers: {
					'Authorization': authToken,
				}
			});
			if ( resp && resp.ok ) {
				respParsed = await resp.json();
			}
		} catch (e) {
			console.log('Cannot fetch token from oracle', e);
			return undefined;
		}

		if ( !respParsed || !respParsed.length || 'error' in respParsed ) {
			try {
				const resp = await fetch(url1155, {
					headers: {
						'Authorization': authToken,
					}
				});
				if ( resp && resp.ok ) {
					respParsed = await resp.json();
				}
			} catch (e) {
				console.log('Cannot fetch token from oracle', e);
				return undefined;
			}
		}
	}

	if ( !respParsed || !respParsed.length || 'error' in respParsed ) {
		console.log('Cannot find token niether 721 nor 1155 from oracle');
		return undefined;
	}


	const foundUserToken = respParsed.find((item: APIWrappedToken) => {
		if ( !params.userAddress ) { return false; }
		return item.owner.toLowerCase() === params.userAddress.toLowerCase()
	});

	if ( foundUserToken ) {
		return await parseAPIToken(foundUserToken, params.chainId);
	}

	return await parseAPIToken(respParsed[0], params.chainId);

}

export const fetchOriginalTokenById = async (params: {
	chainId: number,
	contractAddress: string,
	tokenId: string,
	userAddress?: string,
	assetType?: _AssetType
}):Promise<DiscoveredToken | undefined> => {

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		return undefined;
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return undefined; }

	const url721 = urljoin(BASE_URL, `/discover/721/${params.chainId}/${params.contractAddress}/${params.tokenId}`);
	const url1155 = urljoin(BASE_URL, `/discover/1155/${params.chainId}/${params.contractAddress}/${params.tokenId}`);

	let respParsed: Array<DiscoveredToken> | undefined;

	let url = '';
	if ( params.assetType ) {
		if ( params.assetType === _AssetType.ERC721 ) {
			url = url721;
		}
		if ( params.assetType === _AssetType.ERC1155 ) {
			url = url1155;
		}

		try {
			const resp = await fetch(url, {
				headers: {
					'Authorization': authToken,
				}
			});
			respParsed = await resp.json();
		} catch (e) {
			console.log('Cannot fetch token from oracle', e);
			return undefined;
		}
	} else {

		try {
			const resp = await fetch(url721, {
				headers: {
					'Authorization': authToken,
				}
			});
			if ( resp && resp.ok ) {
				respParsed = await resp.json();
			}
		} catch (e) {
			console.log('Cannot fetch token from oracle', e);
			return undefined;
		}

		if ( !respParsed || !respParsed.length || 'error' in respParsed ) {
			try {
				const resp = await fetch(url1155, {
					headers: {
						'Authorization': authToken,
					}
				});
				if ( resp && resp.ok ) {
					respParsed = await resp.json();
				}
			} catch (e) {
				console.log('Cannot fetch token from oracle', e);
				return undefined;
			}
		}

		if ( !respParsed || !respParsed.length || 'error' in respParsed ) {
			console.log('Cannot find token niether 721 nor 1155 from oracle');
			return undefined;
		}
	}

	if ( !respParsed || 'error' in respParsed ) { return undefined; }

	const foundUserToken = respParsed.find((item: DiscoveredToken) => {
		if ( !params.userAddress ) { return false; }
		return item.owner.toLowerCase() === params.userAddress.toLowerCase()
	});

	if ( foundUserToken ) {
		return foundUserToken;
	}

	return respParsed[0];
}

export const getKioskDisplayInfo = async (params: {
	chainId: number,
	contractAddress: string,
	displayName: string,
}) => {
	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		return undefined;
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return undefined; }

	const nameHashed = getDisplayHash(params.displayName);

	const url = urljoin(BASE_URL, `/kiosk/display/${params.chainId}/${params.contractAddress}/${nameHashed}`);

	let respParsed;
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});
		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		return undefined;
	}

	return respParsed;
}

export const getKioskDisplayItems = async (params: {
	store: any,
	chainId: number,
	contractAddress: string,
	displayName: string,
	page: number,
}) => {

	params.store.dispatch(setTokensLoading());

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		params.store.dispatch(unsetTokensLoading());
		return undefined;
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return undefined; }

	const nameHashed = getDisplayHash(params.displayName);

	const url = urljoin(BASE_URL, `/kiosk/tokens/${params.chainId}/${params.contractAddress}/${nameHashed}?page=${params.page}&size=${config.tokensOnPage}`);

	let respParsed: Array<APIKioskItem> | undefined = undefined;
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});
		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
	} catch (e) {
		params.store.dispatch(unsetTokensLoading());
		console.log('Cannot fetch token from oracle', e);
		return undefined;
	}

	if ( !respParsed || ( !respParsed.length && params.page === 1 ) ) {
		params.store.dispatch(unsetTokensLoading());
		return undefined;
	}

	if ( respParsed.length === config.tokensOnPage ) {
		setTimeout(() => {
			getKioskDisplayItems({
				...params,
				page: params.page + 1,
			})
		}, 1000);
	}

	try {
		respParsed?.forEach(async (item: APIKioskItem) => {
			let name = '';
			let image = '';
			let imageRaw = '';
			let description = '';
			let tokenUrlProcessed = '';
			let tokenUrlBodyRaw = '';
			let tokenUrlBodyParsed;

			if ( item.token_uri ) {
				try {
					tokenUrlProcessed = processSwarmUrl(item.token_uri);
					const tokenUrlResp = await fetch(tokenUrlProcessed);
					tokenUrlBodyRaw = await tokenUrlResp.text();
					tokenUrlBodyParsed = await JSON.parse(tokenUrlBodyRaw);
					name = tokenUrlBodyParsed.name || '';
					image = processSwarmUrl(tokenUrlBodyParsed.image) || '';
					imageRaw = tokenUrlBodyParsed.image || '';
					description = tokenUrlBodyParsed.description || '';
				} catch(e) {
					console.log('Cannot fetch tokenUrl', e);
				}
			} else {
				const authToken = await createAuthToken();
				fetch(urljoin(BASE_URL, `/update_token_uri/${params.chainId}/${item.contract_address}/${item.token_id}`), {
					headers: { 'Authorization': authToken, }
				});
			}

			const collateral = decodeCollaterals(item.collateral);
			collateral.forEach((iitem) => {
				getERC20Token({
					...params,
					contractAddress: iitem.address
				});
			});
			const locks = decodeLocks(item.locks);
			const prices = item.prices
			.filter((iitem) => {
				return !(new BigNumber(iitem.amount).eq(0))
			})
			.map((iitem, idx) => {
				getERC20Token({
					...params,
					contractAddress: iitem.payWith
				});
				return decodePrice(iitem, idx);
			});

			const discounts = []
			const displayDiscount = params.store.getState().displayDiscount;
			if ( displayDiscount ) {
				discounts.push(displayDiscount.discount);
			}

			const token: WrappedTokenType = {
				contractAddress: item.contract_address,
				tokenId: item.token_id,
				assetType: decodeAssetTypeFromIndex(`${item.asset_type}`),
				collateral,
				locks,
				description,
				image,
				imageRaw,
				name,
				tokenUrl: tokenUrlProcessed,
				tokenUrlRaw: item.token_uri,
				tokenUrlRawJSON: tokenUrlBodyRaw,
				prices,
				discounts,
			};

			params.store.dispatch(wrappedTokensAdd(token));
			params.store.dispatch(unsetTokensLoading());
		});
	} catch(e) { console.log('error', respParsed); }
}

export const getERC20Token = async (params: {
	store: any,
	chainId: number,
	contractAddress: string,
	metamaskAdapter?: MetamaskAdapter,
}) => {

	const foundTokenPending = params.store.getState().erc20TokensPending.find((item: string) => { return item === params.contractAddress.toLowerCase() });
	if ( foundTokenPending ) { return undefined; }

	const foundToken = params.store.getState().erc20Tokens.find((item: ERC20ParamsType) => { return item.address.toLowerCase() === params.contractAddress.toLowerCase() });
	if ( foundToken ) { return foundToken }

	params.store.dispatch(addERC20Pending(params.contractAddress));

	if ( params.contractAddress === '0x0000000000000000000000000000000000000000' ) { return undefined; }

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		params.store.dispatch(unsetTokensLoading());
		if ( params.metamaskAdapter ) {
			getERC20Params(
				params.metamaskAdapter.web3,
				params.store,
				params.contractAddress,
				params.metamaskAdapter.userAddress,
				params.store.getState().launchpadContract,
			)
		}
		return undefined;
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		console.log('No oracle base url in .env');
		if ( params.metamaskAdapter ) {
			getERC20Params(
				params.metamaskAdapter.web3,
				params.store,
				params.contractAddress,
				params.metamaskAdapter.userAddress,
				params.store.getState().launchpadContract,
			)
		}
		return undefined;
	}

	const url = urljoin(BASE_URL, `/erc20/${params.chainId}/${params.contractAddress}`);

	let respParsed: APIERC20Item | undefined = undefined;
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		if ( resp.status === 404 ) {
			const authToken = await createAuthToken();
			const url = urljoin(BASE_URL, `/update_token/${params.chainId}/2/${params.contractAddress}`);
			fetch(url, {
				headers: {
					'Authorization': authToken,
				}
			});
			if ( params.metamaskAdapter ) {
				getERC20Params(
					params.metamaskAdapter.web3,
					params.store,
					params.contractAddress,
					params.metamaskAdapter.userAddress,
					params.store.getState().launchpadContract,
				)
			}
			return undefined;
		}

		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
	} catch (e) {
		params.store.dispatch(unsetTokensLoading());
		console.log('Cannot fetch token from oracle', e);
		if ( params.metamaskAdapter ) {
			getERC20Params(
				params.metamaskAdapter.web3,
				params.store,
				params.contractAddress,
				params.metamaskAdapter.userAddress,
				params.store.getState().launchpadContract,
			)
		}
		return undefined;
	}

	if ( !respParsed ) {
		params.store.dispatch(unsetTokensLoading());
		if ( params.metamaskAdapter ) {
			getERC20Params(
				params.metamaskAdapter.web3,
				params.store,
				params.contractAddress,
				params.metamaskAdapter.userAddress,
				params.store.getState().launchpadContract,
			)
		}
		return undefined;
	}

	let balance = new BigNumber(0);
	let allowance = new BigNumber(0);

	if ( params.metamaskAdapter && params.store.getState().metamaskAdapter.logged ) {
		getERC20Balance(
			params.metamaskAdapter.web3,
			params.store,
			params.contractAddress,
			params.metamaskAdapter.userAddress,
			params.store.getState().launchpadContract
		)
	}

	let icon = default_icon;
	try { icon = require(`../../static/pics/coins/${respParsed.symbol.toLowerCase()}.jpeg`     ).default } catch (ignored) {}
	try { icon = require(`../../static/pics/coins/${respParsed.symbol.toLowerCase()}.jpg`      ).default } catch (ignored) {}
	try { icon = require(`../../static/pics/coins/${respParsed.symbol.toLowerCase()}.png`      ).default } catch (ignored) {}
	try { icon = require(`../../static/pics/coins/${respParsed.symbol.toLowerCase()}.svg`      ).default } catch (ignored) {}
	try { icon = require(`../../static/pics/coins/${params.contractAddress.toLowerCase()}.jpeg`).default } catch (ignored) {}
	try { icon = require(`../../static/pics/coins/${params.contractAddress.toLowerCase()}.jpg` ).default } catch (ignored) {}
	try { icon = require(`../../static/pics/coins/${params.contractAddress.toLowerCase()}.png` ).default } catch (ignored) {}
	try { icon = require(`../../static/pics/coins/${params.contractAddress.toLowerCase()}.svg` ).default } catch (ignored) {}

	params.store.dispatch(updateERC20ContractParams({
		address: params.contractAddress,
		name: respParsed.name,
		symbol: respParsed.symbol,
		decimals: parseInt(respParsed.decimals),
		icon,
		balance,
		allowance,
	}));
}