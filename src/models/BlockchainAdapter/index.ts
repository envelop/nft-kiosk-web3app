
import MetamaskAdapter        from './metamaskadapter';
import SubscriptionDispatcher from './subscriptionDispatcher';

import {
	getERC20Params,
	getERC20Balance,
	makeERC20Allowance,
} from './erc20contract';

import type { ChainParamsType } from './metamaskadapter';
import type {
	ERC20ParamsType,
} from './erc20contract';

import {
	getDisplayHash,
	getWrappedTokenById,
	getDisplayAssetItems,
	getAssetItemPrice,
	getDisplayDiscount,
	buyAssetItem,
} from './kioskcontract'

import type {
	_Asset,
	_AssetItem,
	CollateralItem,
	_Royalty,
	Royalty,
	RoyaltyInput,
	_Lock,
	Lock,
	_Fee,
	Fee,
	Rules,
	OriginalTokenType,
	WrappedTokenType,
	_Discount,
	Discount,
	_DiscountUntil,
	DiscountUntil,
	_KioskAssetItem,
	_Price,
	Price,
} from './_types';
import {
	_AssetType,
	_DiscountType,
	LockType,
	encodeCollaterals,
	decodeCollaterals,
	encodeRoyalties,
	decodeRoyalties,
	getLockType,
	encodeLocks,
	decodeLocks,
	encodeFees,
	decodeFees,
	encodeRules,
	decodeRules,
	decodeAssetTypeFromIndex,
	decodeWrappedToken,
	getNativeCollateral,
	assetTypeToString,
	SubscriptionPayOption,
	SubscriptionTariff,
	decodeDiscountTypeFromIndex,
	discountTypeToString,
	encodeDiscount,
	decodeDiscount,
	encodeDiscountUntil,
	decodeDiscountUntil,
	encodePrice,
	decodePrice,
} from './_types';
import {
	getERC721Token,
	loadERC721TokenAll,
	saveERC721Token,
	removeERC721Token,
	transferERC721Token,
	mintToken,
	checkApprovalERC721Token,
}  from './erc721contract';

export {
	_AssetType,
	_DiscountType,
	LockType,
	MetamaskAdapter,
	getERC20Params,
	getERC20Balance,
	makeERC20Allowance,
	getERC721Token,
	loadERC721TokenAll,
	saveERC721Token,
	removeERC721Token,
	transferERC721Token,
	mintToken,
	checkApprovalERC721Token,
	decodeAssetTypeFromIndex,
	decodeWrappedToken,

	SubscriptionDispatcher,

	encodeCollaterals,
	decodeCollaterals,
	encodeRoyalties,
	decodeRoyalties,
	getLockType,
	encodeLocks,
	decodeLocks,
	encodeFees,
	decodeFees,
	encodeRules,
	decodeRules,
	getNativeCollateral,

	assetTypeToString,

	getDisplayHash,
	getWrappedTokenById,
	getDisplayAssetItems,
	getAssetItemPrice,
	getDisplayDiscount,

	decodeDiscountTypeFromIndex,
	discountTypeToString,
	encodeDiscount,
	decodeDiscount,
	encodeDiscountUntil,
	decodeDiscountUntil,
	encodePrice,
	decodePrice,
	buyAssetItem,
};

export type {
	_Asset,
	_AssetItem,
	_Fee,
	_Lock,
	_Royalty,
	Royalty,
	RoyaltyInput,
	Lock,
	Fee,
	OriginalTokenType,

	ChainParamsType,
	ERC20ParamsType,
	WrappedTokenType,
	CollateralItem,
	Rules,
	SubscriptionPayOption,
	SubscriptionTariff,

	_Discount,
	Discount,
	_DiscountUntil,
	DiscountUntil,
	_KioskAssetItem,
	_Price,
	Price,
}