
import Web3         from 'web3';
import erc20_abi    from '../../abis/_erc20.json';

import default_icon from '../../static/pics/coins/_default.svg';

import { updateERC20ContractParams } from '../../reducers';

import BigNumber from 'bignumber.js';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

export type ERC20ParamsType = {
	address          : string,
	name             : string,
	symbol           : string,
	decimals         : number | undefined,
	icon             : string,
	balance          : BigNumber,
	allowance        : BigNumber,
}

export const getERC20Params = async (web3: Web3, store: any, contractAddress: string, userAddress?: string, allowanceTo?: string) => {
	const contract = new web3.eth.Contract(erc20_abi as any, contractAddress);

	let name        = '';
	let symbol      = '';
	let decimals    = 18;
	let balance     = new BigNumber(0);
	let allowance   = new BigNumber(0);
	let icon        = default_icon;
	try {
		name       = await contract.methods.name().call();
		symbol     = await contract.methods.symbol().call();
		decimals   = await contract.methods.decimals().call();
		if ( userAddress ) {
			balance = new BigNumber(await contract.methods.balanceOf(userAddress).call());
			if ( allowanceTo ) {
				allowance = new BigNumber(await contract.methods.allowance(userAddress, allowanceTo).call());
			}
		}

		try { icon = require(`../../static/pics/coins/${symbol.toLowerCase()}.jpeg`         ).default } catch (ignored) {}
		try { icon = require(`../../static/pics/coins/${symbol.toLowerCase()}.jpg`          ).default } catch (ignored) {}
		try { icon = require(`../../static/pics/coins/${symbol.toLowerCase()}.png`          ).default } catch (ignored) {}
		try { icon = require(`../../static/pics/coins/${symbol.toLowerCase()}.svg`          ).default } catch (ignored) {}
		try { icon = require(`../../static/pics/coins/${contractAddress.toLowerCase()}.jpeg`).default } catch (ignored) {}
		try { icon = require(`../../static/pics/coins/${contractAddress.toLowerCase()}.jpg` ).default } catch (ignored) {}
		try { icon = require(`../../static/pics/coins/${contractAddress.toLowerCase()}.png` ).default } catch (ignored) {}
		try { icon = require(`../../static/pics/coins/${contractAddress.toLowerCase()}.svg` ).default } catch (ignored) {}
	} catch(ignored) { return; }

	store.dispatch(updateERC20ContractParams({
		address: contractAddress,
		name,
		symbol,
		decimals,
		icon,
		balance,
		allowance,
	}));

	return {
		address: contractAddress,
		name,
		symbol,
		decimals,
		icon,
		balance,
		allowance,
	}
}

export const getERC20Balance = async (web3: Web3, store: any, contractAddress: string, userAddress: string, allowanceTo?: string) => {
	const foundToken = store.getState().erc20Tokens.find((item: ERC20ParamsType) => { return item.address.toLowerCase() === contractAddress.toLowerCase() });
	if ( !foundToken ) {
		return await getERC20Params(web3, store, contractAddress, userAddress, allowanceTo || '0x0000000000000000000000000000000000000000')
	}
	const contract = new web3.eth.Contract(erc20_abi as any, contractAddress);

	let balance   = new BigNumber(0);
	let allowance = new BigNumber(0);

	balance   = new BigNumber(await contract.methods.balanceOf(userAddress).call());
	if ( allowanceTo ) {
		allowance = new BigNumber(await contract.methods.allowance(userAddress, allowanceTo).call());
	}


	store.dispatch(updateERC20ContractParams({
		...foundToken,
		balance,
		allowance,
	}));

	return {
		...foundToken,
		balance,
		allowance,
	}
}

export const makeERC20Allowance = async (web3: Web3, contractAddress: string, userAddress: string, amount: BigNumber, addressTo: string) => {
	const contract = new web3.eth.Contract(erc20_abi as any, contractAddress);

	const parsedAmount = new BigNumber(amount).toString() === '-1' ? new BigNumber(10**50).toString() : new BigNumber(amount).toString();
	const result = await contract.methods.approve(addressTo, parsedAmount).send({ from: userAddress });
	return result;
}