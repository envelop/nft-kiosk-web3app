
import Web3 from 'web3';
import WalletConnectProvider from "@walletconnect/web3-provider";
import config from '../../config.json';
import { Contract } from 'web3-eth-contract';

import {
	setError,
	resetAppData,

	metamaskConnectionSuccess,
	metamaskConnectionRejected,
	metamaskConnectionNotInstalled,
	metamaskSetChainParams,

	updateNativeBalance,

	metamaskSetAvailableChains,
	setAuthMethod,
	setLoading,
	unsetLoading,
	clearError,
	requestChain,
	unsetAuthMethod,
	setInfo,
	clearInfo,
} from '../../reducers';

import {
	getERC20Params,
	ERC20ParamsType,
} from './';

import default_icon from '../../static/pics/coins/_default.svg';

import {
	localStorageGet,
	localStorageRemove,
	localStorageSet
} from '../_utils';

import { SafeAppProvider } from '@gnosis.pm/safe-apps-provider';
import SafeAppsSDK from '@gnosis.pm/safe-apps-sdk';

import BigNumber from 'bignumber.js';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

type MetamaskAdapterPropsType = {
	store: any,
	t    : any,
}
export type ChainParamsType = {
	chainId?                      : number | undefined,
	chainName                     : string,
	chainRPCUrl                   : string,
	networkTokenTicket            : string,
	EIPPrefix                     : string,
	networkTokenDecimals          : number | undefined,
	networkTokenIcon?             : string | undefined,
	networkIcon?                  : string | undefined,
	isTestNetwork                 : Boolean;
	explorerBaseUrl               : string;
	explorerName                  : string;
	launchpadContract             : string;
	defaultDisplayName?           : string,
	INFO_MESSAGES?                : Array<{
		text: string;
		link_url: string;
		link_text: string;
		isClosable: boolean;
	}>;
};

export default class MetamaskAdapter {

	store           : any;
	web3!           : Web3;
	wcProvider!     : any;
	availiableChains: Array<ChainParamsType>;
	chainId?        : number;
	chainConfig!    : ChainParamsType;
	userAddress!    : string;
	whitelistContract!: Contract;
	unsubscribe     : () => {};
	chainChangeRequested: boolean;

	t: any;

	accessAtempts   : number;

	constructor(props: MetamaskAdapterPropsType) {
		this.store = props.store;
		this.t     = props.t;
		this.availiableChains = config.CHAIN_SPECIFIC_DATA;
		this.store.dispatch(metamaskSetAvailableChains(
			this.availiableChains.map((item) => {
				let networkIcon   = default_icon;
				try { networkIcon = require(`../../static/pics/networks/${item.chainId}.jpeg`).default } catch (ignored) {}
				try { networkIcon = require(`../../static/pics/networks/${item.chainId}.jpg` ).default } catch (ignored) {}
				try { networkIcon = require(`../../static/pics/networks/${item.chainId}.png` ).default } catch (ignored) {}
				try { networkIcon = require(`../../static/pics/networks/${item.chainId}.svg` ).default } catch (ignored) {}
				return {
					...item,
					networkIcon
				}
			})
		));
		this.chainChangeRequested = false;

		this.accessAtempts = 0;

		const sdk = new SafeAppsSDK();
		Promise.race([
			new Promise((res) => { sdk.safe.getInfo().then((data) => { res(data) }) }),
			new Promise((res, rej) => { setTimeout(() => { rej() }, 500) }),
		])
			.then(() => {
				this.connect();
			})
			.catch((ignored) => {
				if ( localStorageGet('provider_type') === 'GNOSIS' ) {
					this.store.dispatch(unsetAuthMethod());
					localStorageRemove('provider_type');
				}
			})

		this.unsubscribe = this.store.subscribe(() => {
			if ( !this.chainId && this.store.getState().selectedChain ) {
				this.chainId = this.store.getState().selectedChain;
				localStorageSet('lastLogin', JSON.stringify({ chainId: this.chainId, userAddress: this.userAddress }));

				this.getChainConfg();
			}
			if ( this.store.getState().metamaskAdapter.logged && !this.chainChangeRequested && this.store.getState().metamaskAdapter.requestChainId && this.chainId && this.store.getState().metamaskAdapter.requestChainId !== this.chainId ) {
				this.chainChangeRequested = true;
				this.store.dispatch(setInfo({
					text: `You are trying to open chain which does not match one selected in metamask`,
					buttons: [
						{
							text: this.t('Switch network'),
							clickFunc: () => {
								(window as any).ethereum.request({
									method: 'wallet_switchEthereumChain',
									params: [{ chainId: '0x' + Number(this.store.getState().metamaskAdapter.requestChainId).toString(16) }], // chainId must be in hexadecimal numbers
								})
							}
						},
						{
							text: this.t('Continue with current'),
							clickFunc: async () => {
								this.store.dispatch(requestChain( undefined ));
								this.chainChangeRequested = false;
								window.location.href = '/launchpad';
								await this.getChainConfg();
								this.store.dispatch(clearInfo());
							}
						},
					],
					links: undefined
				}));
			}
		})
	}
	async connect() {
		let method = this.store.getState().metamaskAdapter.authMethod;
		const sdk = new SafeAppsSDK();

		try {
			await Promise.race([
				new Promise((res) => { sdk.safe.getInfo().then((data) => { res(data) }) }),
				new Promise((res, rej) => { setTimeout(() => { rej() }, 500) }),
			]);
			method = 'GNOSIS'
		} catch(ignored) {}

		this.store.dispatch(setLoading({ msg: this.t('Waiting for metamask login') }));
		if ( method === 'METAMASK' ) {
			try {
				if ( !(window as any).ethereum ) {
					// console.log((window as any).ethereum)
					this.store.dispatch(unsetLoading());
					this.store.dispatch(metamaskConnectionNotInstalled());
					this.store.dispatch(setError({
						text: this.t('No access to metamask'),
						buttons: [{
							text: this.t('Download extension or mobile app'),
							clickFunc: () => { window.open('https://metamask.io/download.html', "_blank"); }
						},
						{
							text: this.t('Close'),
							clickFunc: () => {
								this.store.dispatch(clearError());
							}
						}],
						links: undefined
					}));
					return;
				} else {
					await (window as any).ethereum.request({ method: 'eth_requestAccounts' });
				}
			} catch(e) {
				this.store.dispatch(unsetLoading());
				this.accessAtempts++;
				console.log('Cannot connect to metamask:', e);

				if ( this.accessAtempts < 3 ) {
					setTimeout(() => { this.connect() }, 100);
				} else {
					this.accessAtempts = 0;
					this.store.dispatch(setError({
						text: this.t('You should grant access in metamask'),
						buttons: [{
							text: this.t('Try again'),
							clickFunc: () => { this.connect(); this.store.dispatch(clearError()); }
						}],
						links: undefined
					}));
					this.store.dispatch(metamaskConnectionRejected());
					localStorageRemove('provider_type');
				}

				return;
			}

			try {
				this.web3 = new Web3( (window as any).ethereum );
				localStorageSet('provider_type', 'METAMASK');
			} catch(e: any) {
				this.store.dispatch(unsetLoading());
				this.store.dispatch(setError({
					text: this.t('Cannot connect to metamask'),
					buttons: [{
						text: this.t('Try again'),
						clickFunc: () => { this.connect() }
					}],
					links: undefined
				}));
				console.log(`Cannot connect to metamask: ${e.toString()}`);
				this.store.dispatch(metamaskConnectionRejected());
				localStorageRemove('provider_type');
			}

		}
		if ( method === 'WALLET_CONNECT' ) {
			const urls: any = {};
			this.availiableChains.forEach((item) => {
				if (!item.chainId) { return; }
				urls[item.chainId] = item.chainRPCUrl;
			});

			try {
				this.wcProvider = new WalletConnectProvider({
					rpc: urls,
				});
				await this.wcProvider.enable();
			} catch(e) {
				this.store.dispatch(unsetLoading());
				console.log('Cannot connect to wallet connect:', e);

				this.store.dispatch(setError({
					text: this.t('You should grant access in your wallet'),
					buttons: [{
						text: this.t('Try again'),
						clickFunc: () => {this.connect(); this.store.dispatch(clearError()); }
					},
					{
						text: this.t('Close'),
						clickFunc: () => { this.store.dispatch(clearError()); }
					}],
					links: undefined
				}));
				this.store.dispatch(metamaskConnectionRejected());
				localStorageRemove('provider_type');

				return;
			}
			try {
				this.web3 = new Web3( this.wcProvider );
				localStorageSet('provider_type', 'WALLET_CONNECT');
			} catch(e: any) {
				this.store.dispatch(unsetLoading());
				this.store.dispatch(setError({
					text: this.t('Cannot connect to walletconnect'),
					buttons: [{
						text: this.t('Try again'),
						clickFunc: () => { this.connect() }
					}],
					links: undefined
				}));
				localStorageRemove('provider_type');
				console.log(`Cannot connect to walletconnect: ${e.toString()}`);
				this.store.dispatch(metamaskConnectionRejected());
			}
		}

		if ( method === 'GNOSIS' ) {
			try {
				const sdk = new SafeAppsSDK();
				const safe = await sdk.safe.getInfo();
				this.web3 = new Web3(new SafeAppProvider(safe, sdk) as any);
				localStorageSet('provider_type', 'GNOSIS');
			} catch(e: any) {
				this.store.dispatch(unsetLoading());
				this.store.dispatch(setError({
					text: this.t('Cannot connect to gnosis safe app'),
					buttons: [{
						text: this.t('Try again'),
						clickFunc: () => { this.connect() }
					}],
					links: undefined
				}));
				localStorageRemove('provider_type');
				console.log(`Cannot connect to gnosis safe app: ${e.toString()}`);
				this.store.dispatch(metamaskConnectionRejected());
			}
		}

		console.log('web3', this.web3);
		const accounts = await this.web3.eth.getAccounts();

		this.userAddress = accounts[0];
		this.store.dispatch(metamaskConnectionSuccess({
			address: this.userAddress,
		}));
		await this.getChainId();
		this.store.dispatch(unsetLoading());
		this.fetchNativeBalance();
		this.updateChainListener(method);
	}
	async getChainId() {
		const chainId = await this.web3.eth.getChainId()
		this.chainId = chainId;

		localStorageSet('lastLogin', JSON.stringify({ chainId: this.chainId, userAddress: this.userAddress }));

		await this.checkUrlChain();
	}
	async checkUrlChain() {
		if (
			( this.store.getState().metamaskAdapter.requestChainId && this.store.getState().metamaskAdapter.requestChainId !== this.chainId ) ||
			( this.store.getState().selectedChain && this.store.getState().selectedChain !== this.chainId )
		) {
			this.store.dispatch(setInfo({
				text: `You are trying to open chain which does not match one selected in metamask`,
				buttons: [
					{
						text: this.t('Switch network'),
						clickFunc: () => {
							(window as any).ethereum.request({
								method: 'wallet_switchEthereumChain',
								params: [{ chainId: '0x' + Number(this.store.getState().metamaskAdapter.requestChainId || this.store.getState().selectedChain).toString(16) }], // chainId must be in hexadecimal numbers
							})
						}
					},
					{
						text: this.t('Continue with current'),
						clickFunc: async () => {
							window.location.replace('/launchpad');
						}
					},
				],
				links: undefined
			}));
		} else {
			await this.getChainConfg();
		}
	}
	async getChainConfg() {

		let foundChain = this.availiableChains.filter((item: ChainParamsType) => { return item.chainId === this.chainId });
		if ( !foundChain.length ) {
			const chosenAuthMethod = this.store.getState().metamaskAdapter.authMethod;
			this.store.dispatch(resetAppData());
			this.store.dispatch(setAuthMethod(chosenAuthMethod));
			localStorageRemove('walletconnect');
			const availableChainsStr = this.availiableChains.map((item) => { return item.isTestNetwork ? `${item.chainName} (testnet)` : item.chainName }).join(', ');
			this.store.dispatch(setInfo({
				text: `${this.t('Unsupported chain. Please choose from')}: ${ availableChainsStr }`,
				buttons: [{
					text: this.t('Connect again'),
					clickFunc: () => { this.connect() }
				}],
				links: undefined
			}));
			console.log('Cannot load domain info');
			return;
		}

		let tokenIcon   = default_icon;
		try { tokenIcon = require(`../../static/pics/coins/${foundChain[0].networkTokenTicket.toLowerCase()}.jpeg`).default } catch (ignored) {}
		try { tokenIcon = require(`../../static/pics/coins/${foundChain[0].networkTokenTicket.toLowerCase()}.jpg` ).default } catch (ignored) {}
		try { tokenIcon = require(`../../static/pics/coins/${foundChain[0].networkTokenTicket.toLowerCase()}.png` ).default } catch (ignored) {}
		try { tokenIcon = require(`../../static/pics/coins/${foundChain[0].networkTokenTicket.toLowerCase()}.svg` ).default } catch (ignored) {}

		let networkIcon   = default_icon;
		try { networkIcon = require(`../../static/pics/networks/${foundChain[0].chainId}.jpeg`).default } catch (ignored) {}
		try { networkIcon = require(`../../static/pics/networks/${foundChain[0].chainId}.jpg` ).default } catch (ignored) {}
		try { networkIcon = require(`../../static/pics/networks/${foundChain[0].chainId}.png` ).default } catch (ignored) {}
		try { networkIcon = require(`../../static/pics/networks/${foundChain[0].chainId}.svg` ).default } catch (ignored) {}

		this.chainConfig = {
			chainId             : this.chainId,
			chainName           : foundChain[0].chainName,
			chainRPCUrl         : foundChain[0].chainRPCUrl,
			networkTokenTicket  : foundChain[0].networkTokenTicket,
			EIPPrefix           : foundChain[0].EIPPrefix,
			networkTokenDecimals: foundChain[0].networkTokenDecimals,
			networkTokenIcon    : tokenIcon,
			networkIcon         : networkIcon,
			isTestNetwork       : foundChain[0].isTestNetwork,
			explorerBaseUrl     : foundChain[0].explorerBaseUrl,
			explorerName        : foundChain[0].explorerName,
			launchpadContract   : foundChain[0].launchpadContract,
			INFO_MESSAGES       : foundChain[0].INFO_MESSAGES
		};

		this.store.dispatch(metamaskSetChainParams( this.chainConfig ));
	}
	chainUpdated() {
		if ( window.location.href.toLowerCase().includes('wrap') ) {
			window.location.href = '/list';
		} else {
			window.location.reload();
		}
	}
	updateChainListener(authMethod: string) {
		if ( authMethod === 'METAMASK' ) {
			(window as any).ethereum.on('chainChanged',    () => { this.chainUpdated() });
			(window as any).ethereum.on('accountsChanged', () => { this.chainUpdated() });
		}
		if ( authMethod === 'WALLET_CONNECT' ) {
			this.wcProvider.on("accountsChanged",          () => { this.chainUpdated() });
			this.wcProvider.on("chainChanged",             () => { this.chainUpdated() });
		}

		// if ( authMethod === 'METAMASK' ) {
		// 	(window as any).ethereum.on('chainChanged',    () => { this.store.dispatch(setSelectedChain({ chainId: this.chainId || 0 })); });
		// 	(window as any).ethereum.on('accountsChanged', () => { this.chainUpdated() });
		// }
		// if ( authMethod === 'WALLET_CONNECT' ) {
		// 	this.wcProvider.on("accountsChanged",          () => { this.chainUpdated() });
		// 	this.wcProvider.on("chainChanged",             () => { this.store.dispatch(setSelectedChain({ chainId: this.chainId || 0 })); });
		// }
	}

	async fetchNativeBalance() {
		const balance = new BigNumber(await this.web3.eth.getBalance(this.userAddress));
		this.store.dispatch(updateNativeBalance({ balance }));
	}

	async addERC20Contracts(erc20Contract: string) {
		if ( erc20Contract === '' ) { return; }
		if ( erc20Contract === '0x0000000000000000000000000000000000000000' ) { return; }

		const foundToken = this.store.getState().erc20Tokens.find((item: ERC20ParamsType) => { return item.address.toLowerCase() === erc20Contract.toLowerCase() });
		if ( foundToken ) { return; }

		getERC20Params(this.web3, this.store, erc20Contract);
	}

}