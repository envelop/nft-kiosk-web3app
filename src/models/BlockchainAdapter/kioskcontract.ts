
import Web3         from 'web3';
import {
	ERC20ParamsType,
	getERC20Balance,
	makeERC20Allowance,
	MetamaskAdapter,
	_AssetItem,
	_AssetType,
} from '.';

import {
	getABI,
	processSwarmUrl
} from '../_utils';

import {
	addDisplayDiscount,
	clearInfo,
	setError,
	setInfo,
	setLoading,
	setNoTokens,
	setTokensLoading,
	unsetLoading,
	unsetTokensLoading,
	wrappedTokensAdd,
	wrappedTokensUpdatePrice
} from '../../reducers'

import {
	CollateralItem,
	decodeAssetTypeFromIndex,
	decodeDiscount,
	decodeDiscountUntil,
	decodePrice,
	decodeWrappedToken,
	Discount,
	originalToWrapped,
	Price,
	WrappedTokenType,
	_Discount,
	_KioskAssetItem,
	_Price
} from './_types';

import { getERC20Token } from '../APIService/apiservice';

import BigNumber  from 'bignumber.js';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

export const getDisplayHash = (name: string): string => {
	const web3 = new Web3();
	const encodedStr = web3.eth.abi.encodeParameter('string', name);
	return web3.utils.keccak256(encodedStr);
}

export const getWrappedTokenById = async (params: {
	metamaskAdapter: MetamaskAdapter,
	assetType: _AssetType,
	contractAddress: string,
	tokenId: string
}): Promise<WrappedTokenType> => {

	let contract;
	if ( params.assetType === _AssetType.ERC1155 ) {
		let wnft1155ABI = getABI(params.metamaskAdapter.chainId || 0, params.contractAddress, '_wnft1155');
		contract = new params.metamaskAdapter.web3.eth.Contract(wnft1155ABI, params.contractAddress);
	} else {
		let wnft721ABI = getABI(params.metamaskAdapter.chainId || 0, params.contractAddress, '_wnft721');
		contract = new params.metamaskAdapter.web3.eth.Contract(wnft721ABI, params.contractAddress);
	}

	let tokenUrl;
	let tokenUrlRaw;
	let owner   = undefined;
	let amount = undefined;
	let totalSupply = undefined;
	let name;
	let description;
	let image;
	let image_url;
	let imageRaw;

	if ( params.assetType === _AssetType.ERC721 ) {
		tokenUrlRaw = await contract.methods.tokenURI(params.tokenId).call();
		tokenUrl    = processSwarmUrl(tokenUrlRaw);
		owner       = await contract.methods.ownerOf(params.tokenId).call()
	} else {
		tokenUrlRaw = await contract.methods.uri(params.tokenId).call();
		tokenUrl    = processSwarmUrl(tokenUrlRaw);
		try {
			amount = new BigNumber(await contract.methods.balanceOf(params.metamaskAdapter.userAddress, params.tokenId).call());
		} catch(e) { console.log(`Cannot get amount of token ${params.tokenId}`) }
		try {
			totalSupply = new BigNumber(await contract.methods.totalSupply(params.tokenId).call());
		} catch(e) { console.log(`Cannot get amount of token ${params.tokenId}`) }
	}

	let tokenUrlRes;
	let tokenUrlRawJSON;
	let tokenParsed;
	try {
		tokenUrlRes     = await fetch(tokenUrl);
		tokenUrlRawJSON = await tokenUrlRes.text();
		tokenParsed     = JSON.parse(tokenUrlRawJSON);
		name            = tokenParsed.name        || '';
		description     = tokenParsed.description || '';
		image           = '';
		imageRaw        = '';
		if ( tokenParsed.image     ) { image = processSwarmUrl(tokenParsed.image    ); imageRaw = tokenParsed.image    ; }
		if ( tokenParsed.image_url ) { image = processSwarmUrl(tokenParsed.image_url); imageRaw = tokenParsed.image_url; }
	} catch(e) {
		console.log('Cannot fetch tokenUrl', e);
	}

	let wrappedToken;
	try {
		wrappedToken = await contract.methods.wnftInfo(params.tokenId).call();
	} catch(notWNFT: any) {
		const originalToken = originalToWrapped({
			chainId: params.metamaskAdapter.chainId || 0,
			assetType: params.assetType,
			contractAddress: params.contractAddress,
			tokenId: params.tokenId,

			owner,
			amount,
			totalSupply,
			description,
			image,
			image_url,
			imageRaw,
			name,
			tokenUrl,
			tokenUrlRaw,
			tokenUrlRawJSON,
		});
		return originalToken;
	}

	let wrappedTokenParsed = decodeWrappedToken({
		inWNFT         : wrappedToken,
		owner          : owner,
		chainId        : params.metamaskAdapter.chainId || 0,
		contractAddress: params.contractAddress,
		tokenId        : params.tokenId,
		assetType      : params.assetType,
		amount         : amount,
		totalSupply    : totalSupply,
		tokenUrl       : tokenUrl,
		tokenUrlRaw    : tokenUrlRaw,
		tokenUrlRawJSON: tokenUrlRawJSON,
	});
	if ( wrappedTokenParsed.collateral ) {
		wrappedTokenParsed.collateral.forEach((item: CollateralItem) => { if ( item.assetType === _AssetType.ERC20 ) { params.metamaskAdapter.addERC20Contracts(item.address) } })
	}
	// wrappedTokenParsed = await fillCollateralsImages(params.metamaskAdapter, wrappedTokenParsed, params.metamaskAdapter.userAddress);

	// console.log('raw', wrappedToken);
	// console.log('parsed', wrappedTokenParsed);

	if ( wrappedTokenParsed.originalTokenInfo && wrappedTokenParsed.originalTokenInfo.assetType === _AssetType.ERC721 ) {
		let erc721ABI = getABI(params.metamaskAdapter.chainId || 0, params.contractAddress, '_erc721');
		const contract = new params.metamaskAdapter.web3.eth.Contract(erc721ABI as any, wrappedTokenParsed.originalTokenInfo.contractAddress);
		try {
			const originalTokenName = await contract.methods.name().call();
			wrappedTokenParsed = {
				...wrappedTokenParsed,
				originalTokenInfo: {
					...wrappedTokenParsed.originalTokenInfo,
					name: originalTokenName
				}
			}
		} catch(ignored) {}
	}

	return wrappedTokenParsed;
}

export const getDisplayAssetItems = async (metamaskAdapter: MetamaskAdapter, store: any, contractAddress: string, displayName: string) => {

	store.dispatch(setTokensLoading());

	let kioskABI;
	try {
		kioskABI = getABI(metamaskAdapter.chainId || 0, contractAddress, 'kiosk');
	} catch(e) {
		console.log(`Cannot load ${contractAddress} kiosk abi:`, e);
		throw new Error(`Cannot load kiosk abi`);
	}
	const contract = new metamaskAdapter.web3.eth.Contract(kioskABI, contractAddress);
	const nameHashed = getDisplayHash(displayName);
	const assetItems: Array<_KioskAssetItem> = await contract.methods.getDisplayAssetItems(nameHashed).call();

	getDisplayDiscount(metamaskAdapter, store, contractAddress, displayName)

	if ( !assetItems.length ) {
		store.dispatch(setNoTokens());
	}

	assetItems.forEach(async (item: any) => {
		// try {
			const token = await getWrappedTokenById({
				metamaskAdapter: metamaskAdapter,
				assetType: decodeAssetTypeFromIndex(item.nft.asset.assetType),
				contractAddress: item.nft.asset.contractAddress,
				tokenId: item.nft.tokenId
			});
			const prices = await contract.methods.getAssetItemPricesAndDiscounts(
				item.nft,
				metamaskAdapter.userAddress,
				'0x0000000000000000000000000000000000000000',
				'0x0000000000000000000000000000000000000000'
			).call();
			token.prices = prices[0].map((iitem: _Price, idx: number) => {
				getERC20Token({
					store,
					metamaskAdapter,
					chainId: metamaskAdapter.chainId || 0,
					contractAddress: iitem.payWith,
				});
				return decodePrice(iitem, idx)
			});
			token.discounts = prices[1].map((iitem: _Discount) => {
				return decodeDiscount(iitem)
			});
			store.dispatch(wrappedTokensAdd(token));

			store.dispatch(unsetTokensLoading());
		// } catch(e: any) {
		// 	console.log('Cannot fetch token:', item.nft.asset.contractAddress, item.nft.tokenId)
		// }
	});
}

export const getAssetItemPrice = async (params: {
	metamaskAdapter: MetamaskAdapter,
	store: any,
	contractAddress: string,
	assetItem: _AssetItem,
	userAddress?: string,
	promocode?: string,
	referral?: string,
}) => {
	let kioskABI;
	try {
		kioskABI = getABI(params.metamaskAdapter.chainId || 0, params.contractAddress, 'kiosk');
	} catch(e) {
		console.log(`Cannot load ${params.contractAddress} kiosk abi:`, e);
		throw new Error(`Cannot load kiosk abi`);
	}

	const contract = new params.metamaskAdapter.web3.eth.Contract(kioskABI, params.contractAddress);
	const referralToCheck = params.metamaskAdapter.web3.utils.isAddress(params.referral || '0x0000000000000000000000000000000000000000') ? params.referral : '0x0000000000000000000000000000000000000000'

	const pricesNdiscounts = await contract.methods.getAssetItemPricesAndDiscounts(
		params.assetItem,
		params.userAddress || '0x0000000000000000000000000000000000000000',
		referralToCheck,
		params.promocode   || '',
	).call();

	const prices: Array<Price> = pricesNdiscounts[0].map((iitem: _Price, idx: number) => {
		params.metamaskAdapter.addERC20Contracts(iitem.payWith);
		return decodePrice(iitem, idx)
	});
	const discounts: Array<Discount> = pricesNdiscounts[1]
		.map((iitem: _Discount) => {
			return decodeDiscount(iitem)
		})
		.filter((item: any, idx: number, array: any[]) => {
			if ( item.dsctPercent.eq(0) ) { return false; }
			return idx === array.findIndex((iitem) => { return iitem.dsctType === item.dsctType })
		})

	const token = {
		contractAddress: params.assetItem.asset.contractAddress,
		tokenId: params.assetItem.tokenId,
		prices,
		discounts,
	}
	params.store.dispatch(wrappedTokensUpdatePrice(token));
}

export const getDisplayDiscount = async (metamaskAdapter: MetamaskAdapter, store: any, contractAddress: string, displayName: string) => {
	let kioskABI;
	try {
		kioskABI = getABI(metamaskAdapter.chainId || 0, contractAddress, 'kiosk');
	} catch(e) {
		console.log(`Cannot load ${contractAddress} kiosk abi:`, e);
		throw new Error(`Cannot load kiosk abi`);
	}

	const contract = new metamaskAdapter.web3.eth.Contract(kioskABI, contractAddress);
	const nameHashed = getDisplayHash(displayName);
	const now = new BigNumber(new Date().getTime()).dividedBy(1000);

	const displayInfo = await contract.methods.getDisplay(nameHashed).call();
	if ( displayInfo.enable_after ) {
		const enableAfterParsed = new BigNumber(displayInfo.enable_after);
		const now = new BigNumber(new Date().getTime()).dividedBy(1000);

		if ( now.lt(enableAfterParsed) ) {
			store.dispatch(setError({
				text: `Launchpad is not active yet`,
				buttons: [
					{
						text: 'Go to main display',
						clickFunc: async () => {
							window.location.replace(`/launchpad`);
						}
					},
				],
				links: undefined
			}));
			return;
		}
	}

	try {
		let nearestDiscount: any | undefined = undefined;
		const discountUntil = await contract.methods.getDisplayTimeDiscounts(nameHashed).call();
		if ( discountUntil.discounts && discountUntil.discounts.length ) {
			discountUntil.discounts
				.forEach((item: any) => {
					const untilParsed = new BigNumber(item.until);
					if ( untilParsed.lt(now) ) { return; }
					if ( nearestDiscount.until && untilParsed.plus(-now).lt(nearestDiscount.until.plus(-now)) ) {
						nearestDiscount = item;
					}
				});

			if ( nearestDiscount ) {
				const parsedDiscount = { dsctPercent: nearestDiscount.percent, dsctType: nearestDiscount.discount_type, };
				store.dispatch(addDisplayDiscount(decodeDiscountUntil({
					untilDate: nearestDiscount.until,
					discount: parsedDiscount
				})));
			}
		}
	} catch(ignored) {}
	// if ( discountUntil && discountUntil.length ) {
	// 	store.dispatch(addDisplayDiscount(decodeDiscountUntil(discountUntil[0])));
	// }

}

const approveERC20 = async (params: {
	metamaskAdapter: MetamaskAdapter,
	store: any,
	contractAddress: string,
	tokenToBuy: WrappedTokenType,
	tokenToPay: ERC20ParamsType,
	priceAmount: BigNumber,
	priceIdx: number,
	referral?: string,
	promocode?: string,
}) => {
	params.store.dispatch(setLoading({ msg: 'Waiting for approve' }));

	const balance = await getERC20Balance(
		params.metamaskAdapter.web3,
		params.store,
		params.tokenToPay.address,
		params.metamaskAdapter.userAddress,
		params.contractAddress
	);
	if ( balance.balance.lt(params.priceAmount) ) {
		throw new Error('Not enough balance');
	}
	if ( balance.allowance.lt(params.priceAmount) ) {
		await makeERC20Allowance(
			params.metamaskAdapter.web3,
			params.tokenToPay.address,
			params.metamaskAdapter.userAddress,
			params.priceAmount,
			params.contractAddress
		);
	}
}
export const buyAssetItem = async (params: {
	metamaskAdapter: MetamaskAdapter,
	store: any,
	contractAddress: string,
	tokenToBuy: WrappedTokenType,
	tokenToPay: ERC20ParamsType,
	priceAmount: BigNumber,
	priceIdx: number,
	referral?: string,
	promocode?: string,
}) => {
	params.store.dispatch(setLoading({ msg: 'Waiting for checkout' }));
	let kioskABI;
	try {
		kioskABI = getABI(params.metamaskAdapter.chainId || 0, params.contractAddress, 'kiosk');
	} catch(e) {
		console.log(`Cannot load ${params.contractAddress} kiosk abi:`, e);
		params.store.dispatch(unsetLoading());
		params.store.dispatch(setError({
			text: `Cannot load kiosk abi: ${e}`,
			buttons: undefined,
			links: undefined
		}));
		return;
	}

	let nativeValue = new BigNumber(0);
	if ( params.tokenToPay.address.toLowerCase() === '0x0000000000000000000000000000000000000000' ) {
		nativeValue = params.priceAmount
	} else {
		try {
			await approveERC20(params)
		} catch(e: any) {
			console.log('Cannot approve token', e.message || e);
			params.store.dispatch(unsetLoading());
			params.store.dispatch(setError({
				text: `Cannot approve token: ${e.message || e}`,
				buttons: undefined,
				links: undefined
			}));
			return;
		}
	}

	params.store.dispatch(setLoading({ msg: 'Waiting for checkout' }));

	const assetItem: _AssetItem = {
		amount: '0',
		asset: {
			assetType: params.tokenToBuy.assetType,
			contractAddress: params.tokenToBuy.contractAddress,
		},
		tokenId: params.tokenToBuy.tokenId
	}
	const referralToCheck = params.metamaskAdapter.web3.utils.isAddress(params.referral || '0x0000000000000000000000000000000000000000') ? params.referral : '0x0000000000000000000000000000000000000000';

	console.log('Checkout params:', {
		assetItem: assetItem,
		price_idx: params.priceIdx,
		userAddress: params.metamaskAdapter.userAddress,
		referralToCheck,
		promocode: params.promocode || '',
		value: nativeValue,
	});

	const contract = new params.metamaskAdapter.web3.eth.Contract(kioskABI, params.contractAddress);

	const tx = contract.methods.buyAssetItem(
		assetItem,
		params.priceIdx,
		params.metamaskAdapter.userAddress,
		referralToCheck,
		params.promocode || ''
	);

	try {
		await tx.estimateGas({
			from: params.metamaskAdapter.userAddress,
			value: nativeValue,
		});
	} catch(e: any) {
		console.log('Cannot buy before', e.message || e);
		params.store.dispatch(unsetLoading());
		params.store.dispatch(setError({
			text: `Cannot buy token: ${e.message || e}`,
			buttons: undefined,
			links: undefined
		}));
		return;
	}

	let data;
	try {
		data = await tx.send({
			from: params.metamaskAdapter.userAddress,
			value: nativeValue,
		});
	} catch(e: any) {
		console.log('Cannot buy after', e.message || e);
		params.store.dispatch(unsetLoading());
		params.store.dispatch(setError({
			text: `Cannot buy token: ${e.message || e}`,
			buttons: undefined,
			links: undefined
		}));
		return;
	}

	params.store.dispatch(unsetLoading());
	params.store.dispatch(setInfo({
		text: 'Token is Successfully bought',
		buttons: [{
		text: 'Ok',
		clickFunc: () => {
			params.store.dispatch(clearInfo());
			window.location.reload();
		}
		}],
		links: [{
			text: `View on ${params.metamaskAdapter.chainConfig.explorerName}`,
			url: `${params.metamaskAdapter.chainConfig.explorerBaseUrl}/tx/${data.transactionHash}`
		}]
	}));

}