
import React               from 'react';
import {
	withRouter,
	match,
} from 'react-router-dom';
import {
	assetTypeToString,
	ERC20ParamsType,
	MetamaskAdapter,
	WrappedTokenType,
	getDisplayAssetItems,
	getDisplayHash,
	DiscountUntil
} from '../../models/BlockchainAdapter';
import { setTokensLoading } from '../../reducers';

import default_icon     from '../../static/pics/coins/_default.svg';
import icon_loading     from '../../static/pics/loading.svg';
import icon_bg          from '../../static/pics/staking/bg.svg';

import TokenInList from '../TokenInList';

import {
	History,
	Location
} from 'history';
import {
	withTranslation
} from "react-i18next";

import BigNumber from 'bignumber.js';
import urljoin from 'url-join';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

type TokenListProps = {
	store                 : any,
	metamaskAdapter       : MetamaskAdapter,
	showAuthMethodSelector: Function,
	t                     : any,
	match                 : match;
	location              : Location,
	history               : History,
}
type TokenListState = {
	tokenFetchRequested     : boolean,

	decimalsNative          : number,
	symbolNative            : string,
	iconNative              : string,

	tokensOnDisplay         : Array<WrappedTokenType>,
	tokensOnDisplayFiltered : Array<WrappedTokenType>,
	chainId                 : number,
	metamaskLogged          : boolean,
	explorerBaseUrl         : string,

	currentPage             : string,
	wrappedPages            : number,
	wrappedCurrentPage      : number,

	erc20Tokens             : Array<ERC20ParamsType>,

	filterByString          : string,
	filterByToken           : Array<ERC20ParamsType>,
	filterByParams          : {
		timeLock   : boolean,
		fee        : boolean,
		unwrapReady: boolean,
	}

	loadingInProgress       : boolean,

	filterCoinsOpened       : boolean,
	filterSortOpened        : boolean,
	sortBy                  : string,

	openedTokenCard         : undefined | { contractAddress: string, tokenId: string },

	launchpadContract       : string,
	displayName             : string,
	displayTitle            : string,
	displayDescription      : string,
	showWidget              : boolean,
	widgetHeight            : number,

	displayDiscount         : DiscountUntil | undefined;

	now                     : number,
}

class TokenList extends React.Component<TokenListProps, TokenListState> {

	store                 : any;
	unsubscribe!          : Function;
	metamaskAdapter       : MetamaskAdapter;
	showAuthMethodSelector: Function;
	t                     : any;
	copiedHintTimer       : number;
	copiedHintTimeout     : number;
	filterCoinsRef        : React.RefObject<HTMLInputElement>;
	filterSortRef         : React.RefObject<HTMLInputElement>;
	tokensOnPage          : number;
	widgetListenerCreated : boolean;
	updateInterval        : any;

	BASE_URL              : string;

	constructor(props: TokenListProps) {
		super(props);

		this.store                  = props.store;
		this.metamaskAdapter        = props.metamaskAdapter;
		this.showAuthMethodSelector = props.showAuthMethodSelector;
		this.t                      = props.t;
		this.copiedHintTimer        = 0;
		this.tokensOnPage           = 12;
		this.filterCoinsRef         = React.createRef();
		this.filterSortRef          = React.createRef();

		this.copiedHintTimeout      = 2; // s

		this.widgetListenerCreated  = false;
		this.updateInterval = 0;

		this.BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL || '';

		const tokensOnDisplay       = this.store.getState().tokensOnDisplay;

		if (
			this.store.getState().metamaskAdapter.logged &&
			!this.store.getState().loadInProgress && !tokensOnDisplay.length &&
			this.metamaskAdapter.chainConfig &&
			!this.store.getState().noTokens &&
			this.store.getState().launchpadContract
		) {
			this.store.dispatch(setTokensLoading());
			getDisplayAssetItems(this.metamaskAdapter, this.store, this.store.getState().launchpadContract, this.store.getState().displayName)
		}

		this.state = {
			tokenFetchRequested     : false,

			decimalsNative          : this.store.getState().metamaskAdapter.networkTokenDecimals,
			symbolNative            : this.store.getState().metamaskAdapter.networkTokenTicket,
			iconNative              : this.store.getState().metamaskAdapter.networkTokenIcon,

			tokensOnDisplay         : tokensOnDisplay,
			tokensOnDisplayFiltered : tokensOnDisplay,
			chainId                 : this.store.getState().metamaskAdapter.chainId,
			metamaskLogged          : this.store.getState().metamaskAdapter.logged,
			explorerBaseUrl         : this.store.getState().metamaskAdapter.explorerBaseUrl,

			erc20Tokens             : this.store.getState().erc20Tokens || [],
			filterByString          : '',
			filterByToken           : [],
			filterByParams          : {
				timeLock   : false,
				fee        : false,
				unwrapReady: false,
			},

			currentPage           : 'wrapped',
			wrappedPages          : Math.ceil( tokensOnDisplay.length / this.tokensOnPage ),
			wrappedCurrentPage    : 0,

			loadingInProgress     : this.store.getState().loadInProgress,

			filterCoinsOpened     : false,
			filterSortOpened      : false,

			openedTokenCard       : undefined,

			sortBy                : '',

			launchpadContract     : this.store.getState().launchpadContract,
			displayName           : this.store.getState().displayName,
			displayTitle          : this.store.getState().displayTitle,
			displayDescription    : this.store.getState().displayDescription,
			showWidget            : this.store.getState().showWidget,
			widgetHeight          : 0,

			displayDiscount       : this.store.getState().displayDiscount,

			now                   : new Date().getTime(),
		}
	}

	async componentDidMount() {

		this.updateInterval = setInterval(() => this.setState({ now: new Date().getTime() }), 1000);

		if ( !this.widgetListenerCreated ) {
			window.addEventListener("message", (e) => {
				if ( e.data.source && e.data.source === 'envelop-widget' ) {
					this.setState({ widgetHeight: parseInt(`${e.data.payload.height}`) + 10 })
				}
			})
			this.widgetListenerCreated = true;
		}

		this.unsubscribe = this.store.subscribe(() => {

			const tokensOnDisplay         = this.store.getState().tokensOnDisplay;
			const tokensOnDisplayFiltered = this.filterTokens(tokensOnDisplay, this.state.filterByString, this.state.filterByToken, this.state.filterByParams);

			if (
				this.store.getState().metamaskAdapter.logged &&
				!this.store.getState().loadInProgress && !tokensOnDisplay.length &&
				this.metamaskAdapter.chainConfig &&
				!this.store.getState().noTokens &&
				this.store.getState().launchpadContract
			) {
				this.store.dispatch(setTokensLoading());
				getDisplayAssetItems(this.metamaskAdapter, this.store, this.store.getState().launchpadContract, this.store.getState().displayName)
			}


			this.setState({
				decimalsNative          : this.store.getState().metamaskAdapter.networkTokenDecimals,
				iconNative              : this.store.getState().metamaskAdapter.networkTokenIcon,
				symbolNative            : this.store.getState().metamaskAdapter.networkTokenTicket,
				tokensOnDisplay         : tokensOnDisplay,
				tokensOnDisplayFiltered : tokensOnDisplayFiltered,
				chainId                 : this.store.getState().metamaskAdapter.chainId,
				metamaskLogged          : this.store.getState().metamaskAdapter.logged,
				explorerBaseUrl         : this.store.getState().metamaskAdapter.explorerBaseUrl,
				erc20Tokens             : this.store.getState().erc20Tokens || [],
				wrappedPages            : Math.ceil( tokensOnDisplayFiltered.length / this.tokensOnPage ),

				loadingInProgress       : this.store.getState().loadInProgress,

				launchpadContract     : this.store.getState().launchpadContract,
				displayName           : this.store.getState().displayName,
				displayTitle          : this.store.getState().displayTitle,
				displayDescription    : this.store.getState().displayDescription,
				showWidget            : this.store.getState().showWidget,

				displayDiscount       : this.store.getState().displayDiscount,
			});
		});
 	}
	componentWillUnmount() {
		clearInterval(this.updateInterval);
		this.unsubscribe();
	}


	getWidget() {
		if ( !this.state.showWidget ) { return null; }
		if ( !this.state.chainId ) { return null; }
		if ( !this.state.launchpadContract ) { return null; }
		if ( !this.state.displayName ) { return null; }

		const url = urljoin(this.BASE_URL, `/widget/${this.state.chainId}/${this.state.launchpadContract}/${getDisplayHash(this.state.displayName)}`);

		return (
			<div className="lp-iframe">
			<div className="container">
				<div className="row">
					<div className="col-lg-8">
						<iframe
							id="widget"
							title="widget"
							style={{ border: 0 }}
							width="100%"
							height={ this.state.widgetHeight }
							src={ url }
						>
						</iframe>
					</div>
				</div>
			</div>
			</div>
		)
	}
	getDescriptionAndCountdownBlock() {
		if ( this.state.displayTitle === '' && this.state.displayDescription === '' && !this.state.displayDiscount ) { return null; }

		return (
			<div className="lp-desc">
				<div className="row">
					{ this.getDescription() }
					{ this.getCountdown() }
				</div>
			</div>
		)
	}
	getDescription() {
		if ( this.state.displayTitle === '' && this.state.displayDescription === '' ) { return null; }

		return (
			<div className="col-lg-7">
				<h1>{ this.state.displayTitle || '' }</h1>
				<p>{ this.state.displayDescription || '' }</p>
			</div>
		)
	}
	getCountdown() {
		if ( !this.state.displayDiscount ) { return null; }

		const now = new BigNumber(new Date().getTime()).dividedBy(1000);
		const diff = this.state.displayDiscount.untilDate.minus(now);

		if ( diff.lte(0) ) {
			window.location.reload();
			return;
		}

		const days = diff.dividedToIntegerBy(3600).dividedToIntegerBy(24);
		const hours = diff.dividedToIntegerBy(3600).modulo(24);
		const minutes = diff.dividedToIntegerBy(60).modulo(60);
		const seconds = diff.modulo(60);

		return (
			<div
				className={`col-lg-4 ${this.state.displayTitle === '' && this.state.displayDescription === '' ? '' : 'offset-lg-1' }`}
			>
				<div className="lp-countdown">
					<div className="countdown-text">{ this.state.displayDiscount.discount.dsctPercent.toString() }% discount</div>
					<div className="countdown-wrap">
						<div className="countdown">
							<div className="countdown__item"> <span className="digits">{ days.toFixed(0) } </span><span className="unit">days</span></div>
							<div className="countdown__item"> <span className="digits">{ hours.toFixed(0) }  </span><span className="unit">hours</span></div>
							<div className="countdown__item"> <span className="digits">{ minutes.toFixed(0) }  </span><span className="unit">minutes</span></div>
							<div className="countdown__item"> <span className="digits">{ seconds.toFixed(0) }  </span><span className="unit">seconds</span></div>
						</div>
					</div>
				</div>
			</div>
		)
	}

	getWrappedTokensCount() {
		if ( !this.state.tokensOnDisplay || !this.state.tokensOnDisplayFiltered ) { return '' }

		if ( this.state.tokensOnDisplay.length === this.state.tokensOnDisplayFiltered.length ) {
			return ( <span>{ this.state.tokensOnDisplayFiltered.length }</span> )
		}

		return ( <span>{ this.state.tokensOnDisplayFiltered.length } / { this.state.tokensOnDisplay.length }</span> )
	}
	getPageSelector() {
		return (
			<div className="db-section__toggle">
				<button
					className={ `tab ${ this.state.currentPage === 'wrapped' ? 'active' : '' }` }
					onClick={() => { this.setState({ currentPage: 'wrapped' }) }}
				>
					Wrapped
					{ ' ' }
					{
						this.getWrappedTokensCount()
					}
				</button>
			</div>
		)
	}
	// ----- FILTER -----
	// ----- FILTER COINS -----
	filterTokens(
		tokensList    : Array<WrappedTokenType>,
		strQuery      : string,
		tokensQuery   : Array<ERC20ParamsType>,
		filterByParams: {
			timeLock   : boolean,
			fee        : boolean,
			unwrapReady: boolean,
		}
	): Array<WrappedTokenType> {
		const strQueryParsed    = strQuery.toLowerCase();
		const tokensQueryParsed = tokensQuery.map((item) => { return {
			...item,
			address: item.address.toLowerCase(),
			name: item.address.toLowerCase(),
			symbol: item.address.toLowerCase(),
		} });

		const filtered = tokensList
			.filter((item) => {

				if ( strQueryParsed !== '' ) {

					if ( item.contractAddress.toLowerCase().includes(strQueryParsed) ) { return true; }
					if ( `${item.tokenId}`.toLowerCase().includes(strQueryParsed) ) { return true; }

					if ( assetTypeToString(item.assetType, '').toLowerCase().includes(strQueryParsed)) { return true; }

					return false;
				}

				if ( tokensQueryParsed.length ) {

					if ( item.collateral.length ) {
						const collateralFound = tokensQueryParsed.find((iitem) => {
							return !!item.collateral.find((iiitem) => { return iitem.address === iiitem.address.toLowerCase() });
						});
						if ( collateralFound ) { return true; }
					}

					return false;
				}

				return true;
			});

		return filtered;
	}
	getFilterTokenListItem(item: ERC20ParamsType) {
		const foundToken = this.state.filterByToken.find((iitem) => { return iitem.address.toLowerCase() === item.address.toLowerCase() });
		if ( foundToken ) {
			return (
				<li
					key={ item.address }
					className="option selected"
					onClick={() => {
						const filterTokensUpdated = this.state.filterByToken.filter((iitem) => {
							return iitem.address.toLowerCase() !== item.address.toLowerCase();
						});
						const tokensOnDisplayFilteredUpdated = this.filterTokens(this.state.tokensOnDisplay, this.state.filterByString, filterTokensUpdated, this.state.filterByParams);
						this.setState({
							filterByToken           : filterTokensUpdated,
							tokensOnDisplayFiltered : tokensOnDisplayFilteredUpdated,
							wrappedPages            : Math.ceil( tokensOnDisplayFilteredUpdated.length / this.tokensOnPage ),
						});
					}}
				>
					<div className="option-coin">
						<span className="i-coin"><img src={ item.icon || default_icon } alt="" /></span>
						<span className="name">{ item.symbol }</span>
					</div>
				</li>
			)
		} else {
			return (
				<li
					key={ item.address }
					className="option"
					onClick={() => {
						const filterTokensUpdated = [
							...this.state.filterByToken,
							item
						];
						const tokensOnDisplayFilteredUpdated = this.filterTokens(this.state.tokensOnDisplay, this.state.filterByString, filterTokensUpdated, this.state.filterByParams);
						this.setState({
							filterByToken           : filterTokensUpdated,
							tokensOnDisplayFiltered : tokensOnDisplayFilteredUpdated,
							wrappedPages            : Math.ceil( tokensOnDisplayFilteredUpdated.length / this.tokensOnPage ),
						});
					}}
				>
					<div className="option-coin">
						<span className="i-coin"><img src={ item.icon || default_icon } alt="" /></span>
						<span className="name">{ item.symbol }</span>
					</div>
				</li>
			)
		}
	}
	getFilterCoinsList() {
		if ( this.state.filterCoinsOpened ) {

			return (
				<ul className="options-list list-gray">

					{ this.getFilterTokenListItem({
						address    : '0x0000000000000000000000000000000000000000',
						icon       : this.state.iconNative,
						decimals   : this.state.decimalsNative,
						name       : 'native',
						symbol     : this.state.symbolNative,
						balance    : new BigNumber(0),
						allowance  : new BigNumber(0),
					}) }

					{
						this.state.erc20Tokens.map((item) => {
							return this.getFilterTokenListItem(item);
						})
					}

				</ul>
			)
		}
	}
	openFilterCoinsList() {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				if ( !this.filterCoinsRef.current ) { return; }
				const path = e.composedPath ? e.composedPath(): e.path;
				if ( path && path.includes(this.filterCoinsRef.current) ) { return; }
				this.closeFilterCoinsList();
			};
		}, 100);
		this.setState({ filterCoinsOpened: true });
	}
	closeFilterCoinsList() {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = null;
		this.setState({ filterCoinsOpened: false });
	}
	// ----- END FILTER COINS -----
	// ----- FILTER SORT -----
	getFilterSortList() {
		if ( this.state.filterSortOpened ) {
			return (
				<ul className="options-list list-gray" style={{ display: 'block' }}>
					<li
						className="option"
						onClick={() => {
							this.setState({ sortBy: 'Address' });
						}}
					>Address</li>
					<li
						className="option"
						onClick={() => {
							this.setState({ sortBy: 'Price ↓' });
						}}
					>Price ↓</li>
					<li
						className="option"
						onClick={() => {
							this.setState({ sortBy: 'Price ↑' });
						}}
					>Price ↑</li>
				</ul>
			)
		}
	}
	openFilterSortList() {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				if ( !this.filterSortRef.current ) { return; }
				const path = e.composedPath ? e.composedPath(): e.path;
				if ( path && path.includes(this.filterSortRef.current) ) { return; }
				this.closeFilterSortList();
			};
		}, 100);
		this.setState({ filterSortOpened: true });
	}
	closeFilterSortList() {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = null;
		this.setState({ filterSortOpened: false });
	}
	// ----- END FILTER SORT -----
	getFilterBlock() {
		if ( this.state.currentPage !== 'wrapped' ) { return; }
		if ( !this.state.tokensOnDisplay.length ) { return; }
		return (
			<div className="db-filter">
				<div className="row">
					<div className="col-12 col-md-5 col-lg-6 mb-4">
					{/* <div className="col-12 col-md-8 col-lg-8 mb-4"> */}
						<input
							className="input-control control-gray control-search"
							type="text"
							placeholder="NFT Address, ID..."
							value={this.state.filterByString}
							onChange={(e) => {
								const strUpdated = e.target.value;
								const tokensOnDisplayFilteredUpdated = this.filterTokens(this.state.tokensOnDisplay, strUpdated, this.state.filterByToken, this.state.filterByParams);
								this.setState({
									filterByString          : strUpdated,
									tokensOnDisplayFiltered : tokensOnDisplayFilteredUpdated,
									wrappedPages            : Math.ceil( tokensOnDisplayFilteredUpdated.length / this.tokensOnPage ),
									wrappedCurrentPage      : 0,
								});
							}}
						/>
					</div>
					<div className="col-12 col-sm-8 col-md-4 col-lg-4 mb-4">
					{/* <div className="col-12 col-sm-12 col-md-4 col-lg-4 mb-4"> */}
						<div
							className="select-custom select-collateral"
							ref={ this.filterCoinsRef }
						>
							<div
								onClick={() => {
									if ( this.state.filterCoinsOpened ) {
										this.closeFilterCoinsList();
									} else {
										this.openFilterCoinsList();
									}
								}}
								className={`input-control control-gray ${ this.state.filterCoinsOpened ? 'active' : '' }`}
							>
								{
									this.state.filterByToken.length ?
									(
										<span className="coins">
											{
												this.state.filterByToken.map((item) => {
													return ( <span className="i-coin"><img src={ item.icon || default_icon } alt="" /></span> )
												})
											}
										</span>
									): ( <span className="empty">Select collateral tokens</span> )
								}
							</div>
							{ this.getFilterCoinsList() }
						</div>
					</div>
					<div className="col-12 col-sm-4 col-md-3 col-lg-2 mb-4">
						<div
							className="select-custom"
							ref={ this.filterSortRef }
						>
							<div
								className={`input-control control-gray ${ this.state.filterSortOpened ? 'active' : '' }`}
								onClick={() => {
									if ( this.state.filterSortOpened ) {
										this.closeFilterSortList();
									} else {
										this.openFilterSortList();
									}
								}}
							>
								<span className="empty">{ this.state.sortBy === '' ? 'Sort by' : this.state.sortBy }</span>
							</div>
							{ this.getFilterSortList() }
						</div>
					</div>
				</div>
			</div>
		)
	}
	// ----- END FILTER -----

	getTokenPagination() {
		if ( this.state.wrappedPages < 2 ) { return null; }
		return (
			<div className="pagination">
				<ul>
					<li>
						<button
							className={`arrow ${ this.state.wrappedCurrentPage === 0 ? 'disabled' : '' }`}
							onClick={(e) => {
								e.preventDefault();
								if ( this.state.wrappedCurrentPage === 0 ) { return; }
								this.setState({ wrappedCurrentPage: this.state.wrappedCurrentPage - 1 })
							}}
						>
							<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fillRule="evenodd" clipRule="evenodd" d="M24.0684 10.3225C24.504 10.7547 24.5067 11.4582 24.0744 11.8938L15.9623 20.0679L23.9397 28.1062C24.372 28.5418 24.3693 29.2453 23.9338 29.6775C23.4982 30.1098 22.7947 30.1071 22.3624 29.6716L13.6082 20.8505C13.1783 20.4173 13.1783 19.7184 13.6082 19.2852L22.4971 10.3284C22.9294 9.89287 23.6329 9.89019 24.0684 10.3225Z" fill="white"></path>
							</svg>
						</button>
					</li>
					{
						Array.from({ length: this.state.wrappedPages }).map((item, idx) => {
							return (
								<li key={ idx }>
									<button
										className={ this.state.wrappedCurrentPage === idx ? 'active' : ''}
										onClick={(e) => { e.preventDefault(); this.setState({ wrappedCurrentPage: idx }) }}
									>
										{ idx + 1 }
									</button>
								</li>
							)
						})
					}
					<li>
						<button
							className={`arrow ${ this.state.wrappedCurrentPage === (this.state.wrappedPages - 1) ? 'disabled' : '' }`}
							onClick={(e) => {
								e.preventDefault();
								if ( this.state.wrappedCurrentPage === (this.state.wrappedPages - 1) ) { return; }
								this.setState({ wrappedCurrentPage: this.state.wrappedCurrentPage + 1 })
							}}
						>
							<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fillRule="evenodd" clipRule="evenodd" d="M15.9316 29.6775C15.496 29.2453 15.4933 28.5418 15.9256 28.1062L24.0377 19.9321L16.0603 11.8938C15.628 11.4582 15.6307 10.7547 16.0662 10.3225C16.5018 9.89019 17.2053 9.89287 17.6376 10.3284L26.3918 19.1495C26.8217 19.5827 26.8217 20.2816 26.3918 20.7148L17.5029 29.6716C17.0706 30.1071 16.3671 30.1098 15.9316 29.6775Z" fill="white"></path>
							</svg>
						</button>
					</li>
				</ul>
			</div>
		)
	}
	getTokensFooter() {
		if ( this.state.loadingInProgress ) {
			return (
				<div className="lp-list__footer">
					<img className="loading" src={ icon_loading } alt="" />
				</div>
			)
		}

		if ( !this.state.metamaskLogged && !this.state.tokensOnDisplay.length ) {
			return (
				<div className="lp-list__footer">
					<div className="nomore"> <a href="/" onClick={(e) => { e.preventDefault(); this.showAuthMethodSelector(); }}>Connect</a> metamask to load tokens from blockchain</div>
				</div>
			)
		}

		if ( this.state.metamaskLogged && !this.state.tokensOnDisplay.length ) {
			return (
				<div className="lp-list__footer">
					<div className="nomore">There's no more wNFT yet</div>
				</div>
			)
		}

		if ( this.state.tokensOnDisplay.length && !this.state.tokensOnDisplayFiltered.length ) {
			return (
				<div className="lp-list__footer">
					<div className="nomore">No tokens match filter criteria</div>
				</div>
			)
		}
	}
	getTokensBody() {

		let sortedList: Array<WrappedTokenType> = [];

		if ( this.state.sortBy === 'Price ↑' ) {
			sortedList = this.state.tokensOnDisplayFiltered
				.sort((item, prev) => {

					let priceItem = item.prices?.find((iitem) => { return iitem.payWith === '0x0000000000000000000000000000000000000000' });
					if ( !priceItem ) { priceItem = item.prices?.find((iitem) => { return iitem.idx === 0 }) }
					let pricePrev = prev.prices?.find((iitem) => { return iitem.payWith === '0x0000000000000000000000000000000000000000' });
					if ( !pricePrev ) { pricePrev = prev.prices?.find((iitem) => { return iitem.idx === 0 }) }

					if ( !priceItem || !pricePrev ) { return 0 }

					if ( priceItem && !pricePrev ) { return -1 }
					if ( pricePrev && !priceItem ) { return  1 }

					if ( priceItem.amount.lt(pricePrev.amount) ) { return -1 }
					if ( priceItem.amount.gt(pricePrev.amount) ) { return  1 }

					return 0
				});
		}
		if ( this.state.sortBy === 'Price ↓' ) {
			sortedList = this.state.tokensOnDisplayFiltered
				.sort((item, prev) => {

					let priceItem = item.prices?.find((iitem) => { return iitem.payWith === '0x0000000000000000000000000000000000000000' });
					if ( !priceItem ) { priceItem = item.prices?.find((iitem) => { return iitem.idx === 0 }) }
					let pricePrev = prev.prices?.find((iitem) => { return iitem.payWith === '0x0000000000000000000000000000000000000000' });
					if ( !pricePrev ) { pricePrev = prev.prices?.find((iitem) => { return iitem.idx === 0 }) }

					if ( !priceItem || !pricePrev ) { return 0 }

					if ( priceItem && !pricePrev ) { return  1 }
					if ( pricePrev && !priceItem ) { return -1 }

					if ( priceItem.amount.lt(pricePrev.amount) ) { return  1 }
					if ( priceItem.amount.gt(pricePrev.amount) ) { return -1 }

					return 0
				})
		}
		if ( this.state.sortBy === 'Address' || this.state.sortBy === '' ) {
			sortedList = this.state.tokensOnDisplayFiltered
				.sort((item, prev) => {
					if ( item.contractAddress.toLowerCase() < prev.contractAddress.toLowerCase() ) { return -1 }
					if ( item.contractAddress.toLowerCase() > prev.contractAddress.toLowerCase() ) { return  1 }

					if ( prev.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() ) {
						try {
							if ( new BigNumber(item.tokenId).isNaN() || new BigNumber(prev.tokenId).isNaN() ) {
								if ( parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`) ) { return  1 }
								if ( parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`) ) { return -1 }
							}
							const itemTokenIdNumber = new BigNumber(item.tokenId);
							const prevTokenIdNumber = new BigNumber(prev.tokenId);

							if ( itemTokenIdNumber.lt(prevTokenIdNumber) ) { return  1 }
							if ( itemTokenIdNumber.gt(prevTokenIdNumber) ) { return -1 }
						} catch ( ignored ) {
							if ( `${item.tokenId}`.toLowerCase() < `${prev.tokenId}`.toLowerCase() ) { return  1 }
							if ( `${item.tokenId}`.toLowerCase() > `${prev.tokenId}`.toLowerCase() ) { return -1 }
						}
					}

					return 0
				})
		}

		return (
			<React.Fragment>
				<div className="c-row">
					{
						sortedList
							.slice(
								(this.state.wrappedCurrentPage * this.tokensOnPage),
								(this.state.wrappedCurrentPage * this.tokensOnPage) + this.tokensOnPage
							)
							.map((item: WrappedTokenType) => {
								return (
									<div className="c-col" key={ `${item.contractAddress}${item.tokenId}` }>
									<TokenInList
										store = { this.store }
										metamaskAdapter = { this.metamaskAdapter }
										showAuthMethodSelector = { this.showAuthMethodSelector }
										token = { item }
										kioskAddress={ this.store.getState().launchpadContract }
										displayName={ this.store.getState().displayName }
										footerUtils = {{
											opened: (
												!!this.state.openedTokenCard &&
												this.state.openedTokenCard.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() &&
												`${this.state.openedTokenCard.tokenId}`.toLowerCase() === `${item.tokenId}`.toLowerCase()
											),
											emitOpen : (contractAddress, tokenId) => { this.setState({ openedTokenCard: { contractAddress, tokenId } }) },
											emitClose: (contractAddress, tokenId) => { this.setState({ openedTokenCard: undefined }) },
										}}
									/>
									</div>
								)
							})
					}
				</div>
				{ this.getTokenPagination() }
			</React.Fragment>
		)
	}
	getTokensList() {
		return (
			<React.Fragment>
			<div className="container">
				{ this.getFilterBlock() }
				{ this.getTokensBody() }
				{ this.getTokensFooter() }
			</div>
			</React.Fragment>
		)
	}

	render() {
		return (
			<main className="s-main">

				<img className="lp-bg" src={ icon_bg } alt="" />
				{/* { this.getWidget() } */}

				<div className="mt-4">
					<div className="container">
						{ this.getDescriptionAndCountdownBlock() }
						<div className="lp-list">
							<div className="c-row">
								{ this.getTokensList() }
							</div>
						</div>
					</div>
				</div>
			</main>
		)
	}
}

export default withTranslation("translations")(withRouter(TokenList));