
import React               from 'react';
import { withTranslation } from "react-i18next";
import {
	CollateralItem,
	ERC20ParamsType,
	_AssetType,
} from '../../models/BlockchainAdapter';

import default_icon from '../../static/pics/coins/_default.svg';
import default_nft  from '../../static/pics/coins/_default_nft.svg';

import BigNumber from 'bignumber.js';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

type CoinIconViewerProps = {
	store         : any,
	t             : any,
	tokens        : Array<CollateralItem>,

	onClick?      : any,
	onMouseEnter? : any,
	onMouseLeave? : any,
}
type CoinIconViewerState = {
	erc20Tokens: Array<ERC20ParamsType>,
	decimalsNative   : number,
	symbolNative     : string,
	iconNative       : string,
	explorerBaseUrl  : string,
}

class CoinIconViewer extends React.Component<CoinIconViewerProps, CoinIconViewerState> {

	store         : any;
	unsubscribe!  : Function;
	t             : any;

	onClick?      : any;
	onMouseEnter? : any;
	onMouseLeave? : any;

	constructor(props: CoinIconViewerProps) {
		super(props);

		this.store        = props.store;
		this.t            = props.t;

		this.onClick      = props.onClick;
		this.onMouseEnter = props.onMouseEnter;
		this.onMouseLeave = props.onMouseLeave;

		const explorerBaseUrl       = this.store.getState().metamaskAdapter.explorerBaseUrl;
		const decimalsNative        = this.store.getState().metamaskAdapter.networkTokenDecimals;
		const symbolNative          = this.store.getState().metamaskAdapter.networkTokenTicket;
		const iconNative            = this.store.getState().metamaskAdapter.networkTokenIcon;
		const erc20Tokens           = this.store.getState().erc20Tokens;

		this.state = {
			explorerBaseUrl,
			decimalsNative,
			symbolNative,
			iconNative,
			erc20Tokens,
		}
	}

	componentDidMount() {
		this.unsubscribe = this.store.subscribe(() => {

			const explorerBaseUrl       = this.store.getState().metamaskAdapter.explorerBaseUrl;
			const decimalsNative        = this.store.getState().metamaskAdapter.networkTokenDecimals;
			const symbolNative          = this.store.getState().metamaskAdapter.networkTokenTicket;
			const iconNative            = this.store.getState().metamaskAdapter.networkTokenIcon;
			const erc20Tokens           = this.store.getState().erc20Tokens;

			this.setState({
				explorerBaseUrl,

				decimalsNative,
				symbolNative,
				iconNative,

				erc20Tokens,
			});
		});
 	}
	componentWillUnmount() { this.unsubscribe(); }

	getCollateralItem(item: CollateralItem) {

		// native token
		if ( item.assetType === _AssetType.native && item.amount ) {
			return ( <span className="i-coin" key={ 'native' }><img src={ this.state.iconNative || default_icon } alt="" /></span> )
		}

		if ( item.assetType === _AssetType.ERC20 && item.amount ) {
			// Common ERC20
			const foundERC20 = this.state.erc20Tokens.filter((iitem: ERC20ParamsType) => {
				if ( !iitem.address ) { return false; }
				return item.address.toLowerCase() === iitem.address.toLowerCase()
			});
			if ( foundERC20.length ) {
				// known ERC20
				return ( <span className="i-coin" key={ foundERC20[0].address }><img src={ foundERC20[0].icon || default_icon } alt="" /></span> )
			} else {
				// unknown ERC20
				return ( <span className="i-coin" key={ item.address }><img src={ default_icon } alt="" /></span> )
			}
		}

		if ( item.assetType === _AssetType.ERC721 ) {
			return ( <span className="i-coin" key={ `${item.address}${item.tokenId}` }><img src={ item.tokenImg || default_nft } alt="" /></span> )
		}

		if ( item.assetType === _AssetType.ERC1155 ) {
			return ( <span className="i-coin" key={ `${item.address}${item.tokenId}` }><img src={ item.tokenImg || default_nft } alt="" /></span> )
		}

		return null;
	}
	render() {
		return (
			<div className="coins">
				{
					this.props.tokens
						.sort((item, prev) => {
							if ( item.assetType < prev.assetType ) { return -1 }
							if ( item.assetType > prev.assetType ) { return  1 }

							if ( item.address.toLowerCase() < prev.address.toLowerCase() ) { return -1 }
							if ( item.address.toLowerCase() > prev.address.toLowerCase() ) { return  1 }

							if ( item.tokenId && prev.tokenId ) {
								try {
									if ( new BigNumber(item.tokenId).isNaN() || new BigNumber(prev.tokenId).isNaN() ) {
										if ( parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`) ) { return -1 }
										if ( parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`) ) { return  1 }
									}
									const itemTokenIdNumber = new BigNumber(item.tokenId);
									const prevTokenIdNumber = new BigNumber(prev.tokenId);

									if ( itemTokenIdNumber.lt(prevTokenIdNumber) ) { return -1 }
									if ( itemTokenIdNumber.gt(prevTokenIdNumber) ) { return  1 }
								} catch ( ignored ) {
									if ( `${item.tokenId}`.toLowerCase() < `${prev.tokenId}`.toLowerCase() ) { return -1 }
									if ( `${item.tokenId}`.toLowerCase() > `${prev.tokenId}`.toLowerCase() ) { return  1 }
								}
							}

							return 0
						})
						.slice(0, 10)
						.map((item) => { return this.getCollateralItem(item); })
				}
			</div>
		)
	}
}

export default withTranslation("translations")(CoinIconViewer);