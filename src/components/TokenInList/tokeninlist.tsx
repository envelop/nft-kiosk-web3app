import React               from 'react';
import Tippy               from '@tippyjs/react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import {
	withRouter,
	match,
} from 'react-router-dom';
import {
	ERC20ParamsType,
	LockType,
	MetamaskAdapter,
	WrappedTokenType,
	Price,
	Discount,
	_DiscountType
} from '../../models/BlockchainAdapter';

import TokenIconViewer     from '../CoinIconViewer';
import TokenViewer         from '../CoinViewer';
import CoinSelector        from '../CoinSelector';

import icon_logo           from "../../static/pics/logo.svg";
import icon_i_copy         from '../../static/pics/i-copy.svg';
import icon_i_link         from '../../static/pics/i-link.svg';
import icon_i_action       from '../../static/pics/icons/i-dots-hor.svg';

import {
	clearError,
	setError
} from '../../reducers';

import {
	compactString,
	tokenToFloat,
	unixtimeToStr,
} from '../../models/_utils';
import {
	History,
	Location
} from 'history';
import { Trans, withTranslation } from "react-i18next";
import {
	getAssetItemPrice,
	buyAssetItem
} from '../../models/BlockchainAdapter';

import BigNumber from 'bignumber.js';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

type TokenInListProps = {
	store                 : any,
	metamaskAdapter       : MetamaskAdapter,
	token                 : WrappedTokenType,
	showAuthMethodSelector: Function,
	footerMsg?            : string,
	t                     : any,
	match                 : match;
	location              : Location,
	history               : History,
	footerUtils           : {
		opened: boolean,
		emitOpen : (contractAddress: string, tokenId: string) => void,
		emitClose: (contractAddress: string, tokenId: string) => void,
	},
	kioskAddress          : string,
	displayName           : string,
}
type TokenInListState = {
	decimalsNative   : number,
	symbolNative     : string,
	iconNative       : string,
	chainId          : number,
	metamaskLogged   : boolean,
	explorerBaseUrl  : string,
	EIPPrefix        : string,
	userAddress      : string,

	erc20Tokens: Array<ERC20ParamsType>,
	priceTokenToRender: string,

	copiedHintWhere : string,
	menuOpened      : boolean,
	cursorOnCardMenu: boolean,
	tokensListOpened: boolean,

	inputPromocode  : string,
	appliedPromocode: string,
	focusedPromocode: boolean,
	inputReferral   : string,
	appliedReferral : string,
	focusedReferral : boolean,
}

class TokenInList extends React.Component<TokenInListProps, TokenInListState> {

	store                 : any;
	unsubscribe!          : Function;
	metamaskAdapter       : MetamaskAdapter;
	t                     : any;
	copiedHintTimer       : number;
	copiedHintTimeout     : number;
	mintSubscribtion      : any | undefined;

	cursorOnCardMenu      : boolean;
	userMenuBlockRef      : React.RefObject<HTMLInputElement>;
	tokensListBlockRef    : React.RefObject<HTMLInputElement>;

	constructor(props: TokenInListProps) {
		super(props);

		this.store              = props.store;
		this.metamaskAdapter    = props.metamaskAdapter;
		this.t                  = props.t;
		this.copiedHintTimer    = 0;
		this.copiedHintTimeout  = 2; // s

		this.cursorOnCardMenu   = false;
		this.userMenuBlockRef   = React.createRef();
		this.tokensListBlockRef = React.createRef();

		this.state = {
			decimalsNative       : this.store.getState().metamaskAdapter.networkTokenDecimals,
			symbolNative         : this.store.getState().metamaskAdapter.networkTokenTicket,
			iconNative           : this.store.getState().metamaskAdapter.networkTokenIcon,
			chainId              : this.store.getState().metamaskAdapter.chainId,
			metamaskLogged       : this.store.getState().metamaskAdapter.logged,
			explorerBaseUrl      : this.store.getState().metamaskAdapter.explorerBaseUrl,
			EIPPrefix            : this.store.getState().metamaskAdapter.EIPPrefix,
			userAddress          : this.store.getState().account.address,

			erc20Tokens          : this.store.getState().erc20Tokens,
			priceTokenToRender   : '',

			copiedHintWhere      : '',
			menuOpened           : false,
			cursorOnCardMenu     : false,
			tokensListOpened     : false,

			inputPromocode       : '',
			appliedPromocode     : '',
			focusedPromocode     : false,
			inputReferral        : '',
			appliedReferral      : '',
			focusedReferral      : false,
		}
	}

	componentDidMount() {
		this.unsubscribe = this.store.subscribe(() => {
			this.setState({
				decimalsNative       : this.store.getState().metamaskAdapter.networkTokenDecimals,
				iconNative           : this.store.getState().metamaskAdapter.networkTokenIcon,
				symbolNative         : this.store.getState().metamaskAdapter.networkTokenTicket,
				chainId              : this.store.getState().metamaskAdapter.chainId,
				metamaskLogged       : this.store.getState().metamaskAdapter.logged,
				explorerBaseUrl      : this.store.getState().metamaskAdapter.explorerBaseUrl,
				EIPPrefix            : this.store.getState().metamaskAdapter.EIPPrefix,
				erc20Tokens          : this.store.getState().erc20Tokens,
				userAddress          : this.store.getState().account.address,
			});
		});
 	}
	componentWillUnmount() { this.unsubscribe(); }

	// ----- HEADER -----
	getIdBlock() {
		if ( `${this.props.token.tokenId}`.length < 8 ) {
			return (
				<CopyToClipboard
					text={ this.props.token.tokenId }
					onCopy={() => {
						this.setState({
							copiedHintWhere: 'id'
						});
						clearTimeout(this.copiedHintTimer);
						this.copiedHintTimer = window.setTimeout(() => { this.setState({
							copiedHintWhere: ''
						}); }, this.copiedHintTimeout*1000);
					}}
				>
					<button className="btn-copy">
						<span>
							<span className="title">{ this.t('ID') }</span>
							{ compactString(this.props.token.tokenId) }
						</span>
						<img src={icon_i_copy} alt="" />
						{ this.getCopiedHint('id') }
					</button>
				</CopyToClipboard>
			)
		}

		return (
			<CopyToClipboard
				text={ this.props.token.tokenId }
				onCopy={() => {
					this.setState({
						copiedHintWhere: 'id'
					});
					clearTimeout(this.copiedHintTimer);
					this.copiedHintTimer = window.setTimeout(() => { this.setState({
						copiedHintWhere: ''
					}); }, this.copiedHintTimeout*1000);
				}}
			>
				<button className="btn-copy">
					<Tippy
						content={ this.props.token.tokenId }
						appendTo={ document.getElementsByClassName("wrapper")[0] }
						trigger='mouseenter'
						interactive={ false }
						arrow={ false }
						maxWidth={ 512 }
					>
						<span>
							<span className="title">{ this.t('ID') }</span>
							{ compactString(this.props.token.tokenId) }
						</span>
					</Tippy>
					<img src={icon_i_copy} alt="" />
					{ this.getCopiedHint('id') }
				</button>
			</CopyToClipboard>
		)
	}
	getContractAddressBlock() {
		return (
			<CopyToClipboard
				text={ this.props.token.contractAddress }
				onCopy={() => {
					this.setState({
						copiedHintWhere: 'address'
					});
					clearTimeout(this.copiedHintTimer);
					this.copiedHintTimer = window.setTimeout(() => { this.setState({
						copiedHintWhere: ''
					}); }, this.copiedHintTimeout*1000);
				}}
			>
				<button className="btn-copy">
					<Tippy
						content={ this.props.token.contractAddress }
						appendTo={ document.getElementsByClassName("wrapper")[0] }
						trigger='mouseenter'
						interactive={ false }
						arrow={ false }
						maxWidth={ 512 }
					>
						<span>
							<span className="title">{ this.t('ADDRESS') }</span>
							{ compactString(this.props.token.contractAddress) }
						</span>
					</Tippy>
					<img src={icon_i_copy} alt="" />
					{ this.getCopiedHint('address') }
				</button>
			</CopyToClipboard>
		)
	}
	getCardHeader() {
		return (
			<div>
				{ this.getContractAddressBlock() }
				{ this.getIdBlock() }
			</div>
		)
	}
	// ----- MENU -----
	getMenuItemOpen() {
		return (
			<li>
				<button
					onClick={() => {
						window.open(`/token/${this.state.chainId}/${this.props.token.contractAddress}/${this.props.token.tokenId}`, '_blank');
					}}
				>
					View token
				</button>
			</li>
		)
	}
	openCardMenu = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				if ( !this.userMenuBlockRef.current ) { return; }
				const _path = e.composedPath() || e.path;
				if ( _path && _path.includes(this.userMenuBlockRef.current) ) { return; }
				this.closeCardMenu();
			};
		}, 100);
		this.setState({ menuOpened: true });
	}
	closeCardMenu = () => {
		setTimeout(() => {
			if ( this.cursorOnCardMenu ) { return; }
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = null;
			this.setState({ menuOpened: false });
		}, 100);
	}
	getCardMenuList() {
		if ( this.state.menuOpened ) {
			return (
				<ul
					className="list-action"
					onMouseEnter={() => {
						this.openCardMenu();
						this.cursorOnCardMenu = true;
					}}
					onMouseLeave={() => {
						this.cursorOnCardMenu = false;
						this.closeCardMenu();
					}}
				>
					{ this.getMenuItemOpen() }
				</ul>
			)
		}
	}
	getCardMenu() {
		return (
			<div
				className="w-card__action"
				ref={ this.userMenuBlockRef }
			>
				<button
					className="btn-action"
					onMouseEnter={ this.openCardMenu }
					onMouseLeave={ this.closeCardMenu }
					onClick={ this.openCardMenu }
				>
					<img src={icon_i_action} alt="" />
				</button>

				{ this.getCardMenuList() }
			</div>
		)
	}
	// ----- END MENU -----
	// ----- END HEADER -----

	// ----- DATA -----
	closeTokenList = () => {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = null;
		this.setState({ tokensListOpened: false });
	}
	openTokenList = () => {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = (e: any) => {
			if ( !this.tokensListBlockRef.current ) { return; }
			const path = e.composedPath ? e.composedPath(): e.path;
			if ( path && path.includes(this.tokensListBlockRef.current) ) { return; }
			this.closeTokenList();
		};
		this.setState({ tokensListOpened: true });
	}
	getCollateralBlock() {

		if ( !this.props.token.collateral || !this.props.token.collateral.length ) { return null; }

		const ITEMS_TO_SHOW = 5;
		let additionalAssetssQty = this.props.token.collateral.length - ITEMS_TO_SHOW;

		let firstTokenSymbol = undefined;
		if ( this.props.token.collateral.length === 1 ) {
			firstTokenSymbol = this.state.erc20Tokens.find((item) => { return item.address.toLowerCase() === this.props.token.collateral[0].address })?.symbol;
		}

		return (
			<div className="param-row">
				<div className="field-label">{ this.t('Collateral') }</div>
				<div className="field-control field-collateral">
					<div
						className="field-collateral__value"
						ref={ this.tokensListBlockRef }
						onClick={ this.openTokenList }
						onMouseEnter={ this.openTokenList }
						onMouseLeave={ this.closeTokenList }
					>

							<TokenIconViewer
								store  = { this.store }
								tokens = { this.props.token.collateral.slice(0,ITEMS_TO_SHOW) }
							/>
							{
								this.props.token.collateral.length === 0 ?
								(
									<div className="sum">no assets</div>
								) : null
							}
							{
								this.props.token.collateral.length === 1 && this.props.token.collateral[0].amount && firstTokenSymbol ?
								(
									<span
										className="more-assets"
									>{ tokenToFloat(this.props.token.collateral[0].amount).toString() } { firstTokenSymbol }</span>
								) : null
							}
							{
								this.props.token.collateral.length > ITEMS_TO_SHOW ?
								(
									<span className="more-assets">
										{ '+' }
										{ additionalAssetssQty || '' }
										{ ' ' }
										{ additionalAssetssQty > 0 ? ( additionalAssetssQty === 1 ? this.t('asset') : this.t('assets') ) : this.t('no assets') }
									</span>
								) : null
							}
							{
								this.state.tokensListOpened && this.props.token.collateral.length ?
									<TokenViewer
										store  = { this.store }
										tokens = { this.props.token.collateral }
									/>
									: null
							}

						{/* <div className="sum">≈ 34 996.70 ETH</div> */}
					</div>
				</div>
			</div>
		)
	}
	getTimeUnlocklBlock() {

		if ( !this.props.token.locks || !this.props.token.locks.length ) { return null; }

		const lock = this.props.token.locks.find((item) => { return item.lockType === LockType.time });
		if ( !lock ) { return null; }

		return (
			<div className="param-row">
				<div className="field-label">Time unlock</div>
				<div className="field-control"><span>{ unixtimeToStr(lock.param).toString() }</span></div>
				{/* <div className="field-control"><span>100</span></div>
				<div className="field-unit"><span>days</span></div> */}
			</div>
		)
	}
	getPriceBlock() {

		if ( !this.props.token.prices || !this.props.token.prices.length ) {
			return null;
		}

		const tokenToRenderParams = this.getTokenToRenderParams();

		if ( !tokenToRenderParams.priceToRender ) {
			return (
				<div className="param-row">
					<div className="field-label">Price</div>
					<div className="field-control">
						Press buy to update
					</div>
				</div>
			)
		}

		let priceBlock;
		if ( tokenToFloat(tokenToRenderParams.priceToRender?.amount || new BigNumber(0), tokenToRenderParams.tokenToRender?.decimals).lt('0.001') ) {
			priceBlock = (
				<Tippy
					content={ tokenToFloat(tokenToRenderParams.priceToRender?.amount || new BigNumber(0), tokenToRenderParams.tokenToRender?.decimals).toString() }
					appendTo={ document.getElementsByClassName("wrapper")[0] }
					trigger='mouseenter'
					interactive={ false }
					arrow={ false }
					maxWidth={ 512 }
				>
					{
						tokenToRenderParams.basicDiscount && tokenToRenderParams.basicDiscount.dsctPercent.gt(0) ? (
							<span className="discount"> &lt; 0.001 </span>
						) : ( <span> &lt; 0.001 </span> )
					}
				</Tippy>
			)
		} else {
			if ( tokenToRenderParams.basicDiscount && tokenToRenderParams.basicDiscount.dsctPercent.gt(0) ) {
				priceBlock = ( <span className="discount">{ Number(tokenToFloat(tokenToRenderParams.priceToRender?.amount || new BigNumber(0), tokenToRenderParams.tokenToRender?.decimals).toFixed(3)) }</span> )
			} else {
				priceBlock = ( <span>{ Number(tokenToFloat(tokenToRenderParams.priceToRender?.amount || new BigNumber(0), tokenToRenderParams.tokenToRender?.decimals).toFixed(3)) }</span> )
			}
		}

		return (
			<div className="param-row">
				<div className="field-label">Price</div>
				<div className="field-control">
					{ priceBlock }
				</div>
				<CoinSelector
					store         = { this.store }
					tokens        = { tokenToRenderParams.tokenListToRender || [] }
					selectedToken = { tokenToRenderParams.priceToRender?.payWith || ( tokenToRenderParams.tokenListToRender ? tokenToRenderParams.tokenListToRender[0].address : '0x0000000000000000000000000000000000000000' ) }
					onChange      = {(address: string) => {
						this.setState({ priceTokenToRender: address })
					}}
				/>
			</div>
		)
	}
	// ----- END DATA -----

	getDiscountLabel() {
		const foundTimeDiscount = this.props.token.discounts?.find((item) => { return item.dsctType === _DiscountType.TIME });
		if ( foundTimeDiscount && foundTimeDiscount.dsctPercent.gt(0) ) {
			return (
					<div className="discount-label">
						<svg width="15" height="38" viewBox="0 0 15 38" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M0 0H15L4.5 19L15 38H0V0Z" fill="#D84747"></path>
						</svg>
						<span>{ foundTimeDiscount.dsctPercent.toString() }% OFF</span>
					</div>
				)
		}
	}
	getDiscountBlock() {

		if ( !this.props.footerUtils.opened ) { return null; }
		if ( !this.props.token.prices || !this.props.token.prices.length ) { return null; }

		const applyPromocodeOnClick = () => {
			this.setState({ appliedPromocode: this.state.inputPromocode });
			this.checkDiscounts();
		}
		const applyReferralOnClick = () => {
			this.setState({ appliedReferral: this.state.inputReferral });
			this.checkDiscounts();
		}
		const deletePromocodeOnClick = () => {
			this.setState({ inputPromocode: '', appliedPromocode: '' })
			setTimeout(() => { this.checkDiscounts(); }, 100);
		}
		const deleteReferralOnClick = () => {
			this.setState({ inputReferral: '', appliedReferral: '' })
			setTimeout(() => { this.checkDiscounts(); }, 100);
		}

		const tokenToRenderParams = this.getTokenToRenderParams();

		let basicDiscountAmount = '0';
		if ( tokenToRenderParams.priceToRender && tokenToRenderParams.basicDiscount && tokenToRenderParams.tokenToRender ) {
			const discountAmount = tokenToRenderParams.priceToRender?.amount.multipliedBy(tokenToRenderParams.basicDiscount.dsctPercent.dividedBy(100));
			basicDiscountAmount = tokenToFloat(discountAmount, tokenToRenderParams.tokenToRender.decimals).toString();
		}
		let promocodeAmount = '0';
		if ( tokenToRenderParams.priceToRender && tokenToRenderParams.promocodeDiscount && tokenToRenderParams.tokenToRender ) {
			const discountAmount = tokenToRenderParams.priceToRender.amount.multipliedBy(tokenToRenderParams.promocodeDiscount.dsctPercent.dividedBy(100));
			promocodeAmount = tokenToFloat(discountAmount, tokenToRenderParams.tokenToRender.decimals).toString();
		}
		let referralAmount = '0';
		if ( tokenToRenderParams.priceToRender && tokenToRenderParams.referralDiscount && tokenToRenderParams.tokenToRender ) {
			const discountAmount = tokenToRenderParams.priceToRender.amount.multipliedBy(tokenToRenderParams.referralDiscount.dsctPercent.dividedBy(100));
			referralAmount = tokenToFloat(discountAmount, tokenToRenderParams.tokenToRender.decimals).toString();
		}

		return (
			<React.Fragment>
			<div className="w-card__discount-bg"></div>
			<div className="w-card__discount-wrap">
				<button
					className="btn btn-sm btn-outline btn-close"
					onClick={() => { this.props.footerUtils.emitClose(this.props.token.contractAddress, this.props.token.tokenId) }}
				> <span>Close</span>
					<svg className="green" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z"></path>
					</svg>
				</button>
				<div className="w-card__discount">
					<div className="discount__codes">
						<div className="input-promo has-value">
							<input
								className="input-control control-gray"
								type="text"
								placeholder="Promo code"
								onChange={(e) => { this.setState({ inputPromocode: e.target.value }) }}
								value={ this.state.inputPromocode }
								onFocus={() => { this.setState({ focusedPromocode: true }) }}
								onBlur={() => {
									let inputUpdated = this.state.inputPromocode
									if ( this.state.inputPromocode === this.state.appliedPromocode ) {
										inputUpdated = this.state.appliedPromocode;
									}
									this.setState({
										focusedPromocode: false,
										inputPromocode: inputUpdated,
									})
								}}
							/>
							{
								this.state.focusedPromocode ?
								( <button className="btn btn-outline-white" onMouseDown={ applyPromocodeOnClick }>Apply</button> ) :
								this.state.inputPromocode !== '' ? ( <button className="btn btn-outline-white" onMouseDown={ deletePromocodeOnClick }>Delete</button> ) : null
							}
						</div>
						<div className="input-promo has-value">
							<input
								className="input-control control-gray"
								type="text"
								placeholder="Referral address"
								onChange={(e) => { this.setState({ inputReferral: e.target.value }) }}
								value={ this.state.inputReferral }
								onFocus={() => { this.setState({ focusedReferral: true }) }}
								onBlur={() => {
									let inputUpdated = this.state.inputReferral
									if ( this.state.inputReferral === this.state.appliedReferral ) {
										inputUpdated = this.state.appliedReferral;
									}
									this.setState({
										focusedReferral: false,
										inputReferral: inputUpdated,
									})
								}}
							/>
							{
								this.state.focusedReferral ?
								( <button className="btn btn-outline-white" onMouseDown={ applyReferralOnClick }>Apply</button> ) :
								this.state.inputReferral !== '' ? ( <button className="btn btn-outline-white" onMouseDown={ deleteReferralOnClick }>Delete</button> ) : null
							}
						</div>
					</div>
					<div className="discount__total">
						<div className="d-flex"><span>Initial price</span>
							<div>{ tokenToFloat(tokenToRenderParams.priceToRender?.amount || new BigNumber(0), tokenToRenderParams.tokenToRender?.decimals).toString() } {tokenToRenderParams.tokenToRender?.symbol || '0x0000000000000000000000000000000000000000' }</div>
						</div>
						{
							tokenToRenderParams.basicDiscount ? (
							<div className="d-flex text-red">
								<span>
									{ tokenToRenderParams.basicDiscount.dsctPercent.toString() }% OFF
								</span>
								<span>
									-{ basicDiscountAmount }
									{ ' ' }
									{ tokenToRenderParams.tokenToRender?.symbol || compactString(tokenToRenderParams.tokenToRender?.address) || '0x0000000000000000000000000000000000000000' }
								</span>
							</div>) : null
						}
						{
							tokenToRenderParams.promocodeDiscount ? (<div className="d-flex text-red"><span>Promo code</span><span>
								-{ promocodeAmount }
								{ ' ' }
								{ tokenToRenderParams.tokenToRender?.symbol || compactString(tokenToRenderParams.tokenToRender?.address) || '0x0000000000000000000000000000000000000000' }
							</span></div>) : null
						}
						{
							tokenToRenderParams.referralDiscount ? (<div className="d-flex text-red"><span>Referral code</span><span>
								-{referralAmount}
								{ ' ' }
								{ tokenToRenderParams.tokenToRender?.symbol || compactString(tokenToRenderParams.tokenToRender?.address) || '0x0000000000000000000000000000000000000000' }
							</span></div>) : null
						}
					</div>
				</div>
				{ this.getCardFooter() }
			</div>
			</React.Fragment>
		)
	}
	getExtra() {
		return (
			<div className="extra">
				{ this.getCopyButton() }
			</div>
		)
	}
	getMedia() {

		if ( this.props.token.image === undefined ) {
			return (
				<div className="inner">
					<div className="default">
						<img src={ icon_logo } alt="" />
						<span><Trans i18nKey="Loading NON-FUNGIBLE TOKEN Preview" components={[<br />]} /></span>
					</div>
				</div>
			)
		}
		if ( this.props.token.image === '' ) {
			return (
				<div className="inner">
					<div className="default">
						<img src={ icon_logo } alt="" />
						<span><Trans i18nKey="Cannot load NON-FUNGIBLE TOKEN Preview" components={[<br />]} /></span>
					</div>
				</div>
			)
		}

		return (
			<div className="inner">
				<video className="img" src={ this.props.token.image } poster={ this.props.token.image } autoPlay={ true } muted={ true } loop={ true } />
			</div>
		)
	}
	getCardFooter() {
		const tokenToRenderParams = this.getTokenToRenderParams();

		if ( !this.props.token.prices || !this.props.token.prices.length ) { return null; }
		if ( !tokenToRenderParams.checkoutPrice ) { return null; }
		if ( tokenToRenderParams.checkoutPrice.lte(0) ) {
			return (
				<button
					className="btn btn-lg btn-white"
					onClick={() => {
						if ( !tokenToRenderParams.tokenToRender || !tokenToRenderParams.priceToRender || !tokenToRenderParams.checkoutPrice ) {
							return;
						}

						buyAssetItem({
							metamaskAdapter: this.metamaskAdapter,
							store: this.store,
							contractAddress: this.props.kioskAddress,
							tokenToBuy: this.props.token,
							tokenToPay: tokenToRenderParams.tokenToRender,
							priceAmount: tokenToRenderParams.checkoutPrice,
							priceIdx: tokenToRenderParams.priceToRender.idx,
							referral: this.state.appliedReferral !== '' ? this.state.appliedReferral : '0x0000000000000000000000000000000000000000',
							promocode: this.state.appliedPromocode,
						})
					}}
				>Take for free</button>
			)
		}

		return (
			<button
				className="btn btn-lg"
				onClick={() => {
					if ( !tokenToRenderParams.tokenToRender || !tokenToRenderParams.priceToRender || !tokenToRenderParams.checkoutPrice ) {
						return;
					}

					buyAssetItem({
						metamaskAdapter: this.metamaskAdapter,
						store: this.store,
						contractAddress: this.props.kioskAddress,
						tokenToBuy: this.props.token,
						tokenToPay: tokenToRenderParams.tokenToRender,
						priceAmount: tokenToRenderParams.checkoutPrice,
						priceIdx: tokenToRenderParams.priceToRender.idx,
						referral: this.state.appliedReferral !== '' ? this.state.appliedReferral : '0x0000000000000000000000000000000000000000',
						promocode: this.state.appliedPromocode,
					})
				}}
			>Buy for { tokenToFloat(tokenToRenderParams.checkoutPrice, tokenToRenderParams.tokenToRender?.decimals).toString() } { tokenToRenderParams.tokenToRender?.symbol || '0x0000000000000000000000000000000000000000' }</button>
		)
	}
	getWantToBuyBtn() {
		// if ( this.props.footerUtils.opened ) { return null; }

		if ( !this.props.token.prices || !this.props.token.prices.length ) {
			return (
				<div className="w-card__bottom">
					<button
						className="btn btn-gray"
						disabled={ true }
					>No price set</button>
				</div>
			)
		}

		return (
			<div className="w-card__bottom">
				<button
					className="btn btn-gray"
					onClick={() => {
						if ( this.state.metamaskLogged ) {
							this.props.footerUtils.emitOpen(this.props.token.contractAddress, this.props.token.tokenId);
							this.checkDiscounts();
						} else {
							this.store.dispatch(setError({
								text: this.t('You should login with metamask'),
								buttons: [{
									text: this.t('Connect'),
									clickFunc: () => {
										this.props.showAuthMethodSelector();
										this.store.dispatch(clearError());
									}
								},
								{
									text: this.t('Cancel'),
									clickFunc: () => {
										this.store.dispatch(clearError());
									}
								}],
								links: undefined
							}));
						}
					}}
				>Want to buy</button>
			</div>
		)
	}

	getCopiedHint(where: string) {
		if ( this.state.copiedHintWhere === where  ) {
			return (<span className="btn-action-info">{ this.t('Copied') }</span>)
		}
	}
	getCopyButton() {
		return (
			<CopyToClipboard
				text={ `${window.location.origin}/token/${this.state.chainId}/${this.props.token.contractAddress}/${this.props.token.tokenId}` }
				onCopy={() => {
					this.setState({
						copiedHintWhere:'tokenlinkimg'
					});
					clearTimeout(this.copiedHintTimer);
					this.copiedHintTimer = window.setTimeout(() => { this.setState({
						copiedHintWhere: ''
					}); }, this.copiedHintTimeout*1000);
				}}
			>
				<button className="btn-token-link">
					<img src={ icon_i_link } alt="" />
					{ this.getCopiedHint('tokenlinkimg') }
				</button>
			</CopyToClipboard>
		)
	}

	getTokenToRenderParams() {
		let tokenListToRender = [];

		const foundNativePrice = this.props.token.prices?.find((item: Price) => { return item.payWith === '0x0000000000000000000000000000000000000000' });
		if ( foundNativePrice ) {
			tokenListToRender.push({
				address          : '0x0000000000000000000000000000000000000000',
				name             : this.state.symbolNative,
				symbol           : this.state.symbolNative,
				decimals         : this.state.decimalsNative,
				icon             : this.state.iconNative,
				balance          : new BigNumber(0),
				allowance        : new BigNumber(0),
			});
		}

		tokenListToRender = [
			...tokenListToRender,
			...this.state.erc20Tokens.filter((item: ERC20ParamsType) => {
				return !!this.props.token.prices?.find((iitem: Price) => { return iitem.payWith.toLowerCase() === item.address.toLowerCase() });
			})
		]

		if ( !tokenListToRender.length ) { return {}; }
		if ( !this.props.token.prices ) { return {}; }

		let priceToRender: Price | undefined;
		let tokenToRender: ERC20ParamsType | undefined;
		if ( this.state.priceTokenToRender !== '' ) {
			priceToRender = this.props.token.prices?.find((item: Price) => {
				return item.payWith.toLowerCase() === this.state.priceTokenToRender.toLowerCase();
			});
		} else {
			priceToRender = this.props.token.prices[0]
		}
		if ( priceToRender?.payWith === '0x0000000000000000000000000000000000000000' ) {
			tokenToRender = {
				address: '0x0000000000000000000000000000000000000000',
				decimals: this.state.decimalsNative,
				symbol: this.state.symbolNative,
				icon: this.state.iconNative,
				name: this.state.symbolNative,
				balance: new BigNumber(0),
				allowance: new BigNumber(0),
			}
		} else {
			tokenToRender = tokenListToRender.find((item: ERC20ParamsType) => {
				if ( !priceToRender ) { return false; }
				return item.address.toLowerCase() === priceToRender.payWith.toLowerCase();
			});
		}

		const basicDiscount = this.props.token.discounts?.find((item: Discount) => { return item.dsctType === _DiscountType.TIME && item.dsctPercent.gt(0) });
		const promocodeDiscount = this.props.token.discounts?.find((item: Discount) => { return item.dsctType === _DiscountType.PROMO && item.dsctPercent.gt(0) });
		const referralDiscount = this.props.token.discounts?.find((item: Discount) => { return item.dsctType === _DiscountType.REFERRAL && item.dsctPercent.gt(0) });

		let checkoutPrice = priceToRender?.amount;
		let summedDiscount = this.props.token.discounts?.reduce((acc, val) => { return acc.plus(val.dsctPercent) }, new BigNumber(0));
		if ( summedDiscount && summedDiscount.lt(10000) && summedDiscount.gt(0) ) {
			checkoutPrice = priceToRender?.amount.plus(-priceToRender?.amount.multipliedBy(summedDiscount?.dividedBy(100)));
		} else {
			if ( summedDiscount?.gt(10000) ) {
				summedDiscount = new BigNumber(10000);
				checkoutPrice = new BigNumber(0);
			}
			if ( summedDiscount?.lt(0) ) {
				summedDiscount = new BigNumber(0);
			}
		}

		return {
			priceToRender,
			tokenToRender,
			basicDiscount,
			promocodeDiscount,
			referralDiscount,
			checkoutPrice,
			summedDiscount,
			tokenListToRender,
		}
	}
	checkDiscounts() {
		getAssetItemPrice({
			metamaskAdapter: this.metamaskAdapter,
			store: this.store,
			contractAddress: this.props.kioskAddress,
			assetItem: {
				amount: '0',
				tokenId: this.props.token.tokenId,
				asset: {
					assetType: this.props.token.assetType,
					contractAddress: this.props.token.contractAddress,
				}
			},
			userAddress: this.metamaskAdapter.userAddress || '0x0000000000000000000000000000000000000000',
			promocode: this.state.inputPromocode || '0x0000000000000000000000000000000000000000',
			referral: this.state.inputReferral || '0x0000000000000000000000000000000000000000',
		})
	}

	render() {
		return (
			<div className="w-card w-card-preview" key={`${this.props.token.contractAddress}${this.props.token.tokenId}`}>
				<div className="bg">
					{ this.getDiscountLabel() }
					<div className="w-card__meta">
						{ this.getCardHeader() }
						{ this.getCardMenu() }
					</div>

					<div className="w-card__token">
						{/* { this.getExtra() } */}
						{ this.getMedia() }
					</div>

					<div className="w-card__param">
						{ this.getCollateralBlock() }
						{ this.getTimeUnlocklBlock() }
						{ this.getPriceBlock() }
					</div>
				</div>
				{ this.getDiscountBlock() }
				{ this.getWantToBuyBtn() }
			</div>
		)
	}
}

export default withTranslation("translations")(withRouter(TokenInList));